# frozen_string_literal: true

require_relative '../lib/application'

# Connect to the database
ActiveRecord::Base.establish_connection(
  adapter: (RaidTrainer.configuration[:database] || {})[:adapter] || 'sqlite3',
  database: (RaidTrainer.configuration[:database] || {})[:database] || "raid-trainer-#{RaidTrainer.environment}"
)
