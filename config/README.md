# Configuration

These files in this directory pertain to the configuration of the application in
its different modes. It is very much for configuring development or production
environments as a general rule. It is oriented toward the web administrator.
