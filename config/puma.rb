# frozen_string_literal: true

require 'puma'

require_relative '../lib/application'

pwd = Dir.pwd

pidfile "#{pwd}/puma.pid"

puts 'Running server...'

config = RaidTrainer.configuration

host = config[:host] || '0.0.0.0'
port = ENV['RAID_TRAINER_WEB_PORT'] || config[:port] || 9292

workers config[:workers] || 1
threads (config[:'min-threads'] || 1), (config[:'max-threads'] || 1)

bind "tcp://#{host}:#{port}"
