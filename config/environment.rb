# frozen_string_literal: true

require 'sinatra'

# The main class for the application.
class RaidTrainer < Sinatra::Base
  # Do environment specific setup
  require_relative "environments/#{ENV['RACK_ENV'] || 'development'}"

  # Open the database
  require_relative 'database'
end
