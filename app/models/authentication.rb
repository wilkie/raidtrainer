# frozen_string_literal: true

# == Schema Information
#
# Table name: authentications
#
#  id            :integer          not null, primary key
#  actor_id      :integer
#  token         :string
#  refresh_token :string
#  hashed_email  :string
#  service       :string
#  expires       :datetime
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Authentication < ActiveRecord::Base
  belongs_to :actor
end
