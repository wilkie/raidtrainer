# frozen_string_literal: true

# == Schema Information
#
# Table name: slots
#
#  id          :integer          not null, primary key
#  actor_id    :integer
#  event_id    :integer
#  image       :string
#  description :text
#  bio         :text
#  start       :datetime
#  end         :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  hide_end    :boolean
#
require_relative '../uploaders/image'

# This represents a scheduled slot in the event.
class Slot < ActiveRecord::Base
  belongs_to :event, optional: false
  belongs_to :actor, optional: false

  has_many :collaborators, dependent: :delete_all

  mount_uploader :image, ImageUploader

  validates :start, presence: true
  validates :end, comparison: { greater_than_or_equal_to: :start }

  validate :within_event?

  def owned_by?(actor)
    (self.actor == actor) || event.owned_by?(actor)
  end

  def show_end_time?
    !hide_end
  end

  # Whether or not the given actor can edit the time of the slot.
  def can_edit_time?(actor)
    # The event owners can always edit the time of the slot
    return true if event.owned_by?(actor)

    # The slot owner can only edit the time if the event owners allow it
    return true if event.allow_participants_to_edit_time && owned_by?(actor)

    # Otherwise, the given actor cannot edit the time
    false
  end

  def within_event?
    errors.add(:start, I18n.t('errors.must-be-within-event')) if start < event.start

    return false unless self.end > event.end

    errors.add(:end, I18n.t('errors.must-be-within-event'))
  end

  def each_interval(step = 30.minutes)
    (start.to_i..self.end.to_i).step(step) do |i|
      yield Time.at(i).to_datetime
    end
  end

  # Returns the next slot after this one during the event.
  def next
    event.slots.where('start > ?', self.end).order(start: :asc).first
  end
end
