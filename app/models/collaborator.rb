# frozen_string_literal: true

# == Schema Information
#
# Table name: collaborators
#
#  id         :integer          not null, primary key
#  actor_id   :integer
#  slot_id    :integer
#  image      :string
#  bio        :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require_relative '../uploaders/image'

# This attaches an Actor as a secondary participant on a Slot.
class Collaborator < ActiveRecord::Base
  belongs_to :slot, optional: false
  belongs_to :actor, optional: false

  has_one :event, through: :slot

  mount_uploader :image, ImageUploader

  def owned_by?(actor)
    actor && ((self.actor == actor) || (slot.actor == actor) || (actor && actor.id == 1))
  end
end
