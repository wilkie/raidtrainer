# frozen_string_literal: true

# == Schema Information
#
# Table name: co_owners
#
#  id       :integer          not null, primary key
#  actor_id :integer
#  event_id :integer
#
class CoOwner < ActiveRecord::Base
  belongs_to :actor
  belongs_to :event
end
