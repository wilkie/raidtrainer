# frozen_string_literal: true

# == Schema Information
#
# Table name: pages
#
#  id           :integer          not null, primary key
#  event_id     :integer
#  name         :string
#  page_type    :string
#  position     :integer
#  content      :text
#  content_type :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
class Page < ActiveRecord::Base
  belongs_to :event, optional: false
end
