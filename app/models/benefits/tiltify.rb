# == Schema Information
#
# Table name: benefits
#
#  id                :integer          not null, primary key
#  event_id          :integer
#  actor_id          :integer
#  slot_id           :integer
#  team_id           :integer
#  type              :string           not null
#  name              :string
#  cause_id          :string
#  cause_name        :string
#  cause_description :text
#  supporting        :string
#  url               :string
#  service           :string
#  service_id        :string
#  service_slug      :string
#  currency          :string
#  goal              :integer
#  raised            :integer
#  donate_url        :string
#  description       :text
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  image_url         :string
#
module Benefits
  # This represents a benefit goal hosted on Tiltify
  class Tiltify < Benefit
    def ensure_service
      self.service = :tiltify
    end

    def update_for_service!
      info = ::Tiltify.retrieve_info_for_url(url)
      self.description = info['description'] if description.blank?
      self.currency = info['currency_code'] if currency.blank?
      self.goal = (info['goal']['value'].to_f * 100).to_i
      self.raised = (info['total_amount_raised']['value'].to_f * 100).to_i
      self.service_slug = info['slug']
      self.service_id = info['id']
      self.name = info['name'] if name.blank?
      self.donate_url = info['donate_url'] if donate_url.blank?
      self.image_url = info['avatar']['src']
      if self.image_url.nil? || self.image_url.include?('default-cause-avatar.png')
        self.image_url = info['cause']['avatar']['src']
      end

      if info['team_id']
        self.team = Actor.find_by(handle: info['team_id']) || Actor.create_for!(:tiltify, info['team_id'])
      elsif info['user_id']
        self.team = Actor.find_by(handle: info['user_id']) || Actor.create_for!(:tiltify, info['user_id'])
      end

      self.cause_id = info['cause']['id']
      self.cause_name = info['cause']['name'] if cause_name.blank?
      self.cause_description = info['cause']['description'] if cause_description.blank?
      save!
    end

    def fill_for_service
      return unless service_slug.blank?

      info = ::Tiltify.retrieve_info_for_url(url)
      self.description = info['description']
      self.currency = info['currency_code']
      self.goal = (info['goal']['value'].to_f * 100).to_i
      self.raised = (info['total_amount_raised']['value'].to_f * 100).to_i
      self.service_slug = info['slug']
      self.service_id = info['id']
      self.name = info['name']
      self.donate_url = info['donate_url']
      self.image_url = info['avatar']['src']
      if self.image_url.nil? || self.image_url.include?('default-cause-avatar.png')
        self.image_url = info['cause']['avatar']['src']
      end

      if info['team_id']
        self.team = Actor.find_by(handle: info['team_id']) || Actor.create_for!(:tiltify, info['team_id'])
      elsif info['user_id']
        self.team = Actor.find_by(handle: info['user_id']) || Actor.create_for!(:tiltify, info['user_id'])
      end

      self.cause_id = info['cause']['id']
      self.cause_name = info['cause']['name']
      self.cause_description = info['cause']['description']
    end
  end
end
