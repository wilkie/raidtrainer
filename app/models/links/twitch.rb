# == Schema Information
#
# Table name: links
#
#  id          :integer          not null, primary key
#  actor_id    :integer
#  handle      :string
#  name        :string
#  service     :string
#  avatar_url  :string
#  url         :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  type        :string           not null
#  service_id  :string
#
module Links
  # This represents a twitch-based actor profile.
  class Twitch < Link
    def ensure_service
      self.service = :twitch
    end

    def fill_for_service
      data = ::Twitch.retrieve_user_info_by_handle(handle)
      return unless data

      self.name = data['display_name']
      self.description = data['description'] if description.blank?
      self.avatar_url = data['profile_image_url']
      self.url = "https://twitch.tv/#{handle}"
      self.service_id = data['id']

      # Ensure that we have an actor
      return unless actor

      # Check to see if we can update the main Actor
      dirty = false
      %i[name handle avatar_url description].each do |field|
        if actor.send(field).blank?
          actor.send("#{field}=", send(field))
          dirty = true
        end
      end

      actor.save! if dirty
      save!
    end
  end

  Link.register_model(:twitch, Links::Twitch)
end
