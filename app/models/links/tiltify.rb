# == Schema Information
#
# Table name: links
#
#  id          :integer          not null, primary key
#  actor_id    :integer
#  handle      :string
#  name        :string
#  service     :string
#  avatar_url  :string
#  url         :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  type        :string           not null
#  service_id  :string
#
module Links
  # This represents a tiltify-based actor profile.
  class Tiltify < Link
    def ensure_service
      self.service = :tiltify
    end

    def fill_for_service
      data = ::Tiltify.retrieve_info_for(:teams, handle)
      data = ::Tiltify.retrieve_info_for(:users, handle) unless data
      self.name = data['name']
      self.name = data['username'] if self.name.blank?
      self.description = data['description'] if description.blank?
      self.avatar_url = data['avatar']['src']
      self.url = data['url']

      # Ensure that we have an actor
      return unless actor

      # Check to see if we can update the main Actor
      dirty = false
      %i[name handle avatar_url description].each do |field|
        if actor.send(field).blank?
          actor.send("#{field}=", send(field))
          dirty = true
        end
      end

      # Overwrite name if name in actor is the same as handle
      if actor.handle == actor.name
        actor.name = self.name
        dirty = true
      end

      actor.save! if dirty
    end
  end

  Link.register_model(:tiltify, Links::Tiltify)
end
