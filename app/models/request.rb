# frozen_string_literal: true

# == Schema Information
#
# Table name: requests
#
#  id          :integer          not null, primary key
#  actor_id    :integer
#  event_id    :integer
#  start       :datetime
#  end         :datetime
#  note        :text
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  hide_end    :boolean
#
class Request < ActiveRecord::Base
  belongs_to :event
  belongs_to :actor

  validates :start, presence: true
  validates :end, comparison: { greater_than: :start }

  validate :within_event?

  def within_event?
    return false if start.nil?
    return false if self.end.nil?

    errors.add(:start, I18n.t('errors.must-be-within-event')) if start < event.start

    return false unless self.end > event.end

    errors.add(:end, I18n.t('errors.must-be-within-event'))
  end

  def each_interval(step = 30.minutes)
    (start.to_i..self.end.to_i).step(step) do |i|
      yield Time.at(i).to_datetime
    end
  end
end
