# frozen_string_literal: true

# == Schema Information
#
# Table name: actors
#
#  id          :integer          not null, primary key
#  handle      :string
#  name        :string
#  avatar_url  :string
#  timezone    :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
require 'digest'

# This represents any person within the system.
class Actor < ActiveRecord::Base
  has_many :links
  has_many :authentications
  has_many :requests
  has_many :slots
  has_many :collaborators
  has_many :co_owners
  has_many :events, foreign_key: 'owner_id'
  has_many :co_owned_events, through: :co_owners, source: :event, class_name: 'Event'
  has_many :participating_events, through: :slots, source: :event, class_name: 'Event'

  has_one :twitch_link, -> { twitch }, class_name: 'Link'

  validates :handle, presence: true

  # The default avatar
  def self.default_avatar_url
    '/images/person.svg'
  end

  def self.find_by_downcase_name(name)
    Actor.where('lower(name) = ?', name.downcase)&.first
  end

  def self.find_by_downcase_handle(handle)
    Actor.where('lower(handle) = ?', handle.downcase)&.first
  end

  # This will create an Actor (or update it) from an OAuth authorization.
  def self.find_or_create_from_omniauth!(auth_hash)
    # Pull out email
    email = auth_hash.extra.raw_info.email
    hashed_email = Digest::SHA2.new(256).hexdigest(email)
    service = auth_hash.provider

    # Try to find the existing Authentication
    auth = Authentication.where(
      service:,
      hashed_email:
    ).first

    # We might not have an actor if they log in for the first time
    # There is an existing actor if we have an existing authentication
    actor = auth&.actor

    # Pull out the fields from the auth hash
    handle = auth_hash.extra.raw_info.login
    name = auth_hash.extra.raw_info.display_name
    avatar_url = auth_hash.extra.raw_info.profile_image_url
    description = auth_hash.extra.raw_info.description

    # If there is no actor and no auth, we can try to do an Actor
    # lookup. In this case, somebody else told us about an Actor
    # and they are now finally logging in for themselves.

    # We find them by the Link relation
    # actor ||= Link.find_by(handle: handle, service: service)&.actor
    actor ||= Link.where('lower(handle) = ?', handle.downcase).first&.actor

    # Create the actor, if we still need one
    actor ||= Actor.create_for!(service, handle)

    # Create the authentication
    auth ||= Authentication.create!(actor:, service:)

    # Update with the newest token
    auth.update!(
      token: auth_hash.credentials.token,
      refresh_token: auth_hash.credentials.refresh_token,
      hashed_email:,
      expires: auth_hash.credentials.expires_at
    )

    actor
  end

  def self.find_by_downcase_handle_or_create_for!(service, handle)
    find_by_downcase_handle(handle) || create_for!(service, handle)
  end

  def self.create_for!(service, handle)
    actor = Actor.create!(
      handle:,
      name: handle
    )

    Link.model_for_service(service).create!(
      actor:,
      handle:,
      service:,
    )

    actor
  end

  def admin?
    Admin.where(actor: self).any?
  end

  # Get all events this Actor is organizing
  def all_events
    events.or(Event.where(id: co_owned_events))
  end

  def events_participating_in
    Event.where(id: Slot.where(actor_id: id).select(:event_id)).distinct
  end

  def url_for_avatar
    avatar_url || Actor.default_avatar_url
  end
end
