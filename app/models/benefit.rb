# frozen_string_literal: true

# == Schema Information
#
# Table name: benefits
#
#  id                :integer          not null, primary key
#  event_id          :integer
#  actor_id          :integer
#  slot_id           :integer
#  team_id           :integer
#  type              :string           not null
#  name              :string
#  cause_id          :string
#  cause_name        :string
#  cause_description :text
#  supporting        :string
#  url               :string
#  service           :string
#  service_id        :string
#  service_slug      :string
#  currency          :string
#  goal              :integer
#  raised            :integer
#  donate_url        :string
#  description       :text
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  image_url         :string
#

# Represents a donation campaign
class Benefit < ActiveRecord::Base
  # It may belong to an event or slot
  belongs_to :event
  belongs_to :slot

  # It is organized by an Actor, possibly
  belongs_to :actor

  # It is organized by an Actor via 'team', usually
  belongs_to :team, class_name: :Actor

  # Ensure we have the information from the service
  before_save :ensure_filled

  # Ensure we store the service identifier
  before_save :ensure_service

  # Updates missing information with information provided by the service
  def update_for_service!; end

  # Fills the table with information provided by the service
  def fill_for_service; end

  # Ensures that the 'service' field is set to a well-known value
  def ensure_service; end

  # Ensures that we have information filled out via the service itself
  def ensure_filled
    fill_for_service
  end

  def self.create_for!(url:, **kwargs)
    case URI(url).host
    when 'tiltify.com'
      Benefits::Tiltify.create!(url:, **kwargs)
    else
      Benefit.create!(url:, **kwargs)
    end
  end

  def percentage
    raised.to_f / goal.to_f * 100.0
  end

  # Updates the info on a timer, returning a cached response in the interim.
  def poll
    @@redis ||= RedisCache.new
    ret = @@redis.get("benefits-#{id}")
    return ret if ret

    update_for_service!

    rendered = {
      raised: Money.from_cents(raised, currency).format,
      goal: Money.from_cents(goal, currency).format,
    }

    ret = { raised:, goal:, rendered: }.to_json
    @@redis.set("benefits-#{id}", ret)
    @@redis.expire("benefits-#{id}", 10)
    ret
  end
end
