# frozen_string_literal: true

# == Schema Information
#
# Table name: links
#
#  id          :integer          not null, primary key
#  actor_id    :integer
#  handle      :string
#  name        :string
#  service     :string
#  avatar_url  :string
#  url         :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  type        :string           not null
#  service_id  :string
#
require 'uri'
require 'net/http'

# The Link connects an Actor to a service, like Twitch.
class Link < ActiveRecord::Base
  belongs_to :actor

  before_save :ensure_filled

  scope :twitch, -> { where(service: 'twitch') }

  # Ensure we have the information from the service
  before_save :ensure_filled

  # Ensure we store the service identifier
  before_save :ensure_service

  # Fills the table with information provided by the service
  def fill_for_service; end

  # Fills the table with information provided by the service and saves it
  def fill_for_service!
    fill_for_service
    save!
  end

  # Ensures that the 'service' field is set to a well-known value
  def ensure_service; end

  def ensure_filled
    return unless avatar_url.blank?

    fill_for_service
  end

  def self.register_model(service, model)
    @services ||= {}
    @services[service.intern] = model
  end

  def self.model_for_service(service)
    @services ||= {}
    @services[service.intern]
  end
end
