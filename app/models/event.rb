# frozen_string_literal: true

require 'redis'

# == Schema Information
#
# Table name: events
#
#  id                              :integer          not null, primary key
#  owner_id                        :integer
#  co_owner_id                     :integer
#  code                            :string
#  name                            :string
#  url                             :string
#  image                           :string
#  background                      :string
#  service                         :string
#  description                     :text
#  open                            :boolean
#  hidden                          :boolean
#  requests_visible                :boolean
#  start                           :datetime
#  end                             :datetime
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  raid_message                    :text
#  allow_participants_to_edit_time :boolean          default(FALSE)
#  event_type                      :integer          default(0)
#  show_intermission               :boolean
#  display_calendar                :boolean          default(FALSE)
#
require_relative '../uploaders/image'

# Represents an entire event.
class Event < ActiveRecord::Base
  belongs_to :owner, class_name: :Actor

  mount_uploader :image, ImageUploader
  mount_uploader :background, ImageUploader

  has_many :slots
  has_many :requests
  has_many :pages
  has_many :benefits

  has_many :co_owners

  has_many :actors, through: :slots

  validates :name, presence: true
  validates :start, presence: true
  validates :end, comparison: { greater_than: :start }

  before_save :ensure_code

  EVENT_TYPES = {
    RAID_TRAIN: 0,
    JAM: 1,
  }.freeze

  def raid_train?
    event_type == EVENT_TYPES[:RAID_TRAIN]
  end

  def jam?
    event_type == EVENT_TYPES[:JAM]
  end

  def show_intermissions?
    show_intermission
  end

  def self.can_create?(actor)
    return false unless actor
    return false unless actor.admin? || RaidTrainer.configuration['allow-new-events'] != 'admin'
    return false unless !!RaidTrainer.configuration['allow-new-events']
    true
  end

  def owned_by?(actor)
    (owner == actor) || co_owners.where(actor:).any?
  end

  def participating_in?(actor)
    return false unless actor
    participants.where(id: actor.id).any?
  end

  # This code generation code is slightly based on the one in the Code.org
  # codebase. So the functions related to code generation are licensed
  # appropriately to the Apache license found in that project.
  CODE_CHARACTERS = ((('A'..'Z').to_a + ('a'..'z').to_a) - %w[A E I O U a e i o u]).freeze
  BANNED_SUBSTRINGS = %w[CNT DCK DMN FCK PNS PSS SHT TTS].freeze

  def ensure_code
    return unless code.blank?

    100.times do
      possible_code = 6.times.to_a.collect { CODE_CHARACTERS.sample }.join
      next if BANNED_SUBSTRINGS.any? { |substring| possible_code.upcase.include? substring }
      next if Event.where(code: possible_code).any?

      self.code = possible_code
      save!
    end
  end

  def url_for_image
    image.url
  end

  def url_for_background
    background.url || '/images/noise_black.png'
  end

  def unslotted
    requesters.where.not(id: participants.pluck(:id)).preload(:links)
  end

  def slotted
    requesters.where(id: participants.pluck(:id)).preload(:links)
  end

  def participants
    Actor.where(id: slots.distinct.select(:actor_id)).preload(:links)
  end

  def requesters
    Actor.where(id: requests.distinct.select(:actor_id)).preload(:links)
  end

  # Returns a list of slots that are happening at the given time.
  def happening_at(time)
    @slots ||= slots.all.to_a
    @slots.filter do |slot|
      slot.start <= time && slot.end > time
    end
  end

  # Returns a list of requests that are happenning at the given time.
  def requested_at(time)
    @requests ||= requests.all.to_a
    @requests.filter do |request|
      request.start <= time && request.end > time
    end
  end

  def each_interval(step = 30.minutes)
    (start.to_i..self.end.to_i).step(step) do |i|
      yield Time.at(i).to_datetime
    end
  end

  def invalidate_cache!
    @@redis ||= RedisCache.new
    @@redis.del("events-#{id}")
  end

  def cached_page
    @redis ||= RedisCache.new
    @redis.get("events-#{id}")
  end

  def cache_page(page)
    @redis ||= RedisCache.new
    @redis.set("events-#{id}", page)
    @redis.expire("events-#{id}", 60)
  end
end
