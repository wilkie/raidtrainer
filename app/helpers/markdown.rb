# frozen_string_literal: true

class RaidTrainer
  # The base controller class.
  class Controller
    module Helpers
      # Contains the helper to render markdown within the content of the site.
      module MarkdownHelpers
        # Renders the given markdown text.
        #
        # Returns a string containing HTML representing the given markdown.
        def render_markdown(content, options = {})
          # Render it
          engine = Redcarpet::Markdown.new(Redcarpet::Render::HTML, options)
          result = engine.render(content)

          # Ensure that headers are always <h3> or greater
          # TODO: I could also do this by overriding the render class
          result.gsub!('<h5>', '<h6>')
          result.gsub!('<h4>', '<h6>')
          result.gsub!('<h3>', '<h5>')
          result.gsub!('<h2>', '<h4>')
          result.gsub!('<h1>', '<h3>')

          result
        end
      end
    end

    helpers Helpers::MarkdownHelpers
  end
end
