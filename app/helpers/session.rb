# frozen_string_literal: true

class RaidTrainer
  # The base controller class.
  class Controller
    module Helpers
      # Contains helper methods for understanding the session data.
      module SessionHelpers
        # Returns true if somebody is currently logged in.
        #
        # This means that `current_actor` is a valid Actor class.
        def logged_in?
          !!session[:actor]
        end

        # Returns the Actor that is currently authenticated.
        def current_actor
          return unless logged_in?

          @current_actor ||= Actor.find(session[:actor])
        end
      end
    end

    helpers Helpers::SessionHelpers
  end
end
