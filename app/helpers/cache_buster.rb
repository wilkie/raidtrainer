class RaidTrainer
  # The base controller class.
  class Controller
    module Helpers
      # Contains helper methods for understanding the session data.
      module CacheBusterHelpers
        # Returns true if somebody is currently logged in.
        #
        # This means that `current_actor` is a valid Actor class.
        def cache_buster
          @redis ||= RedisCache.new
          ret = @redis.get("cache-buster")

          unless ret
            ret = `git rev-parse HEAD`
            @redis.set("cache-buster", ret)
            @redis.expire("cache-buster", 60)
          end

          ret
        end
      end
    end

    helpers Helpers::CacheBusterHelpers
  end
end
