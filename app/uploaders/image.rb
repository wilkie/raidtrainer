# frozen_string_literal: true

# This class represents the agent that handles uploaded images.
class ImageUploader < CarrierWave::Uploader::Base
  def content_type_allowlist
    [%r{image/}]
  end

  def extension_allowlist
    %w[jpg jpeg gif png]
  end
end
