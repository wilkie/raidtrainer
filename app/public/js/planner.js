(() => {
    // Determine timezone offset
    let date = new Date("1/1/2023 00:00");
    var offset = date.toTimeString().split('GMT')[1].split(' ')[0];
    let hours = parseInt(offset.substring(0, offset.length - 2));

    document.querySelectorAll('table.planner').forEach( (table) => {
        table.setAttribute('hidden', '');
        table.setAttribute('aria-hidden', 'true');
    });

    document.querySelectorAll('table.planner[data-tz-offset="' + hours + '"]').forEach( (table) => {
        table.removeAttribute('hidden');
        table.setAttribute('aria-hidden', 'false');

        // Ensure that the actor captions do not overflow their rows
        table.querySelectorAll('.actor').forEach( (actor) => {
            // Count the cells
            let width = 0;
            let cell = actor.parentNode;
            while (cell) {
                width += cell.getBoundingClientRect().width;
                cell = cell.nextElementSibling;
                if (!cell || cell.getAttribute('class').match(/filled-\d/)[0] !== actor.parentNode.getAttribute('class').match(/filled-\d/)[0]) {
                    cell = null;
                }
            }

            actor.style.overflow = 'hidden';
            actor.style.width = (width - 3) + 'px';
            actor.style.minWidth = (width - 3) + 'px';
        });
    });
})();
