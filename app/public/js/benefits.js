// Simply look for benefit panels and poll them every 10 seconds
(() => {
    window.setInterval( () => {
        document.querySelectorAll('.event-benefit.panel').forEach( (panel) => {
            const raisedElement = panel.querySelector('.fundraiser-status .raised');
            const goalElement = panel.querySelector('.fundraiser-status .goal');
            const barElement = panel.querySelector('.bar');

            const pollURL = panel.getAttribute('data-poll-url');
            fetch(pollURL).then( (response) => {
                return response.json();
            }).then( (data) => {
                raisedElement.textContent = data.rendered.raised;
                raisedElement.setAttribute('data-value', data.raised);
                goalElement.textContent = data.rendered.goal;
                goalElement.setAttribute('data-value', data.goal);

                barElement.style.width = (data.raised / data.goal * 100.0) + '%'
            });
        });
    }, 10000);
})();
