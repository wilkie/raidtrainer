(() => {
  // Allow 'now' to update the time
  const urlParams = new URLSearchParams(window.location.search);

  // Craft the 'up-next' schedule
  let currentDate = urlParams.get('now') ? window.luxon.DateTime.fromISO(urlParams.get('now')) : window.luxon.DateTime.now().toLocal();

  // Adjust time slots and look for the current slots, if any
  function update_calendar() {
    document.querySelectorAll('#event-calendar .calendar-week .calendar-day li.calendar-list-item time.start').forEach( (timeElement) => {
      //if (++foo > 1) return;
      const tzDate = timeElement.getAttribute('datetime');
      const startDate = window.luxon.DateTime.fromISO(tzDate).toLocal();
      const local = startDate.toLocal().toJSDate();
      const itemElement = timeElement.closest('.calendar-list-item');

      // Determine if it should move to an earlier day
      let dateString = local.getFullYear() + '-' + (local.getMonth() + 1).toString().padStart(2, '0') + '-' + local.getDate().toString().padStart(2, '0');
      let dayElement = timeElement.closest('.calendar-day');
      const desiredDate = window.luxon.DateTime.fromISO(dateString);
      const desiredDay = desiredDate.day;
      const designatedDate = window.luxon.DateTime.fromISO(dayElement.getAttribute('data-timestamp'));
      const designatedDay = designatedDate.day;
      if (desiredDay != designatedDay) {
        // Move this to the previous day
        let updatedDay = (
          desiredDate < designatedDate ?
          dayElement.previousElementSibling :
          dayElement.nextElementSibling);

        if (!updatedDay) {
          // Move it to next/last week
          const weekElement = dayElement.closest('.calendar-week');
          let updatedWeek = (
            desiredDate < designatedDate ?
            weekElement.previousElementSibling :
            weekElement.nextElementSibling);
          while (updatedWeek && !updatedWeek.classList.contains('calendar-week')) {
            updatedWeek = (
              desiredDate < designatedDate ?
              updatedWeek.previousElementSibling :
              updatedWeek.nextElementSibling);
          }

          if (updatedWeek) {
            updatedDay = (
              desiredDate < designatedDate ?
              updatedWeek.querySelector('.calendar-day:last-child') :
              updatedWeek.querySelector('.calendar-day:first-child'));
          }
        }

        if (updatedDay) {
          const priorList = updatedDay.querySelector('ol.calendar-list');
          const beforeElement = (
            desiredDay < designatedDay ?
            priorList.querySelector('li.rest') :
            priorList.querySelector('li:first-child'));
          const formerList = timeElement.closest('.calendar-list');
          const formerDay = formerList.closest('.calendar-day');
          priorList.insertBefore(itemElement, beforeElement);
          const slot_id = itemElement.getAttribute('data-slot-id');

          // Make the day we moved from 'empty' if it is now empty
          if (!formerList.querySelector('.calendar-list-item')) {
            formerDay.classList.add('empty');
          }

          // Make the new day not 'empty' if it is
          updatedDay.classList.remove('empty');

          // Move bios, if different week
          if (updatedDay.closest('.calendar-week') != dayElement.closest('.calendar-week')) {
            // Find bio slot
            let week = dayElement.closest('.calendar-week');

            const priorWeek = updatedDay.closest('.calendar-week');
            if (priorWeek.hasAttribute('hidden')) {
              priorWeek.removeAttribute('hidden');
              priorWeek.setAttribute('aria-hidden', 'false');
              priorWeek.nextElementSibling.removeAttribute('hidden');
              priorWeek.nextElementSibling.setAttribute('aria-hidden', 'false');
            }
            const priorSlots = priorWeek.nextElementSibling.querySelector('table.slots');
            const slots = week.nextElementSibling;
            let slot = slots.querySelector(`[data-slot-id="${slot_id}"]`);
            if (slot) {
              // Move this and all other bio slots
              priorSlots.appendChild(slot);
              slot = slot.nextElementSibling;

              while (slot && slot.classList.contains('bio')) {
                priorSlots.appendChild(slot);
              }
            }
          }
        }
      }

      // Did this entry happen before our current date?
      if (startDate <= currentDate) {
        // Is this not the end of it?
        const endElement = itemElement.querySelector('time.end');
        let endDate = startDate.plus({hours: 6});
        if (endElement) {
          endDate = window.luxon.DateTime.fromISO(endElement.getAttribute('datetime'));
        }
        else if (timeElement.classList.contains('raid-train-slot')) {
          // This is a raid train slot, so it ends when the next slot begins
          let nextSlot = itemElement.nextElementSibling;
          if (nextSlot) {
            let nextStart = nextSlot.querySelector('time.start');
            if (nextStart) {
              endDate = window.luxon.DateTime.fromISO(nextStart.getAttribute('datetime'));
            }
          }
        }

        if (endDate > currentDate && !itemElement.classList.contains('set-by-navigation')) {
          // Then it is currently happening
          itemElement.classList.add('current');
          return;
        }
      }

      // Not happening now
      if (!itemElement.classList.contains('set-by-navigation')) {
        itemElement.classList.remove('current');
      }
    });
  }

  document.querySelectorAll('#event-calendar .calendar-week .calendar-day li.calendar-list-item time').forEach( (timeElement) => {
    const tzDate = timeElement.getAttribute('datetime');
    const local = window.luxon.DateTime.fromISO(tzDate).toLocal().toJSDate();

    // Re-render time in local time
    timeElement.textContent = local.toLocaleString(undefined, {
      timeStyle: 'short',
    });

    // Check for date string :(
    if (timeElement.textContent.includes(",")) {
      timeElement.textContent = timeElement.textContent.split(',')[1];
    }
  });

  window.setInterval(update_calendar, 1000);
  update_calendar();

  // Just bind to the calendar items to activate them
  let activeItem = null;
  let activeWeek = null;
  let activeSlot = null;

  const months = document.querySelectorAll('.calendar-month');
  months.forEach( (month) => {
    const items = month.querySelectorAll('li.calendar-list-item');
    items.forEach( (item) => {
      item.addEventListener('click', (e) => {
        const slot_id = item.getAttribute('data-slot-id');
        if (activeItem) {
          activeItem.classList.remove('active');
        }
        if (activeWeek) {
          activeWeek.classList.remove('active-selection');
        }
        if (activeSlot) {
          activeSlot.classList.remove('active');
        }

        activeItem = item;
        activeItem.classList.add('active');

        // Find bio slot
        let week = activeItem.parentNode;
        while(week && !week.classList.contains('calendar-week')) {
          week = week.parentNode;
        }
        activeWeek = week;
        activeWeek.classList.add('active-selection');
        const slots = activeWeek.nextElementSibling;
        const slot = slots.querySelector(`[data-slot-id="${slot_id}"]`);
        if (slot) {
          activeSlot = slot;
          activeSlot.classList.add('active');
        }
      });
    });
  });
})();
