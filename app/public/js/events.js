(() => {
    let currentlyStreaming = {};

    // Handle the top image
    let eventImage = document.querySelector('img.event')
    let eventNav = document.querySelector('nav.event-navigation');
    let imageStyle = null;
    let originalTop = null;
    let originalHeight = null;
    let originalWidth = null;
    let originalPaddingLeft = null;
    let minHeight = null;

    if (eventImage && eventImage.parentNode.classList.contains('temporary')) {
        eventImage.parentNode.classList.remove('temporary');
        eventImage.parentNode.classList.remove('static');
    }

    const eventTimeElement = document.querySelector('#event-name p time.start');
    const eventStartTzDate = eventTimeElement.getAttribute('datetime');
    const eventStartDate = window.luxon.DateTime.fromISO(eventStartTzDate).toLocal();

    // Get the content div
    let content = document.querySelector('.content');

    // Allow 'now' to update the time
    const urlParams = new URLSearchParams(window.location.search);
    const difference = (urlParams.get('now') ? ((new Date(urlParams.get('now'))).valueOf() - (new Date(Date.now())).valueOf()) : 0);

    function reposition_event_image() {
        if (!eventImage.complete) {
            return;
        }

        const nav = document.querySelector('body > nav');
        imageStyle = imageStyle || window.getComputedStyle(eventImage);
        originalTop = originalTop || parseInt(imageStyle.top);
        originalHeight = originalHeight || parseInt(imageStyle.height);
        originalWidth = originalWidth || parseInt(imageStyle.width);
        originalPaddingLeft = originalPaddingLeft || parseInt(imageStyle.paddingLeft);
        minHeight = minHeight || parseInt(imageStyle.minHeight);

        let computedTop = originalTop - content.scrollTop;
        let newTop = Math.max(0, computedTop);
        let newHeight = originalHeight;
        if (computedTop < 0) {
            newHeight += computedTop;
        }
        newHeight = Math.max(newHeight, minHeight);
        let newWidth = originalWidth / originalHeight * newHeight;

        if (!eventImage.classList.contains('static')) {
            eventImage.style.top = newTop + 'px';
            eventImage.style.height = newHeight + 'px';
            eventImage.style.width = newWidth + 'px';

            if (eventImage.parentNode.tagName.toUpperCase() === 'A') {
                eventImage.parentNode.style.height = newHeight + 'px';
            }

            let percentage = 1.0 - ((newHeight - minHeight) / (originalHeight - minHeight));
            eventImage.style.backgroundColor = 'rgba(255.0, 255.0, 255.0, ' + percentage + ')';
            eventImage.style.borderColor = 'rgba(200.0, 128.0, 200.0, ' + percentage + ')';

            // Handle 'banner' navigation buttons
            if (eventNav) {
              eventNav.querySelectorAll('span:first-child').forEach( (el) => {
                el.style.right = (((newWidth / 2.0) + originalPaddingLeft) - 4) + 'px';
              });
              eventNav.querySelectorAll('span:last-child').forEach( (el) => {
                el.style.left = (((newWidth / 2.0) + originalPaddingLeft) - 4) + 'px';
              });
              eventNav.style.opacity = percentage;
            }

            if (percentage >= 1.0) {
              eventImage.classList.add('whole');
            }
            else {
              eventImage.classList.remove('whole');
            }
        }
        eventImage.style.left = 'calc(50% - ' + ((newWidth / 2) + originalPaddingLeft) + 'px)';
    }

    if (eventImage) {
        eventImage.addEventListener('load', () => {
            reposition_event_image();
        });

        // Handle the scroll and the event image
        content.addEventListener('scroll', () => {
            reposition_event_image();
        });

        document.body.addEventListener('load', () => {
            reposition_event_image();
        });

        reposition_event_image();
    }

    const isFirefox = navigator.userAgent.toLowerCase().includes('firefox');
    const isChrome = navigator.userAgent.toLowerCase().includes('chrome');

    function openBio(event) {
        if (event.target.parentNode.classList.contains('description')) {
            let bioRow = event.target.parentNode.parentNode.nextElementSibling;
            let index = parseInt(event.target.getAttribute('data-index'));
            while (index > 0) {
                bioRow = bioRow.nextElementSibling;
                index--;
            }

            bioRow.classList.toggle('open');
            if (bioRow.classList.contains('open')) {
                bioRow.removeAttribute('hidden');
                bioRow.setAttribute('aria-hidden', 'false');
            }
            else {
                bioRow.setAttribute('hidden', '');
                bioRow.setAttribute('aria-hidden', 'true');
            }
        }
        else {
            event.target.parentNode.parentNode.classList.toggle('open');
        }
    }

    document.querySelectorAll('button.button.open-bio').forEach( (openBioButton) => {
        openBioButton.parentNode.parentNode.classList.add('bound');
        openBioButton.addEventListener('click', openBio);

        if (!isFirefox && !isChrome) {
            // Safari, et al, move button to description
            let bioRow = openBioButton.parentNode.parentNode;
            bioRow.setAttribute('hidden', '');
            bioRow.setAttribute('aria-hidden', 'true');
            let i = 0;
            while (true) {
                const description = bioRow.previousElementSibling.querySelector('td.description');
                if (description) {
                    openBioButton.setAttribute('data-index', i);
                    description.appendChild(openBioButton)
                    break;
                }
                bioRow = bioRow.previousElementSibling;
                i++;
            }
        }
    });

    // Fix event start/end
    document.querySelectorAll('#event-name time.start, #event-name time.end').forEach( (timeElement) => {
        const tzDate = timeElement.getAttribute('datetime');
        const local = window.luxon.DateTime.fromISO(tzDate).toLocal().toJSDate();
        timeElement.textContent = local.toLocaleString(undefined, {
            dateStyle: 'long',
            timeStyle: 'short',
        });

        document.querySelectorAll('p.gmt-time').forEach( (gmtTimeLabel) => {
            if (gmtTimeLabel) {
                gmtTimeLabel.setAttribute('hidden', '');
                gmtTimeLabel.setAttribute('aria-hidden', 'true');
            }
        });
        document.querySelectorAll('p.local-time').forEach( (localTimeLabel) => {
            if (localTimeLabel) {
                localTimeLabel.removeAttribute('hidden');
                localTimeLabel.setAttribute('aria-hidden', 'false');
            }
        });
    });

    const countdown = document.querySelector('#countdown');
    let forcibly_started = false;
    let forcibly_started_handle = '';

    // Handle the schedule
    function update_schedule() {
        let last_day = null;
        const eventPanel = document.querySelector('#event-next');
        const eventIsRaidTrain = eventPanel.hasAttribute('data-raidtrain');
        document.querySelectorAll('table.slots tr td.time time').forEach( (timeElement) => {
            const tzDate = timeElement.getAttribute('datetime');
            const local = window.luxon.DateTime.fromISO(tzDate).toLocal().toJSDate();

            let dateString = local.getFullYear() + '-' + (local.getMonth() + 1).toString().padStart(2, '0') + '-' + local.getDate().toString().padStart(2, '0');
            if (!timeElement.classList.contains('end') && (!last_day || last_day != dateString)) {
                // Find a template for this day
                let dayRow = document.querySelector('template.day[datetime="' + dateString + '"]');
                if (dayRow) {
                    let instance = dayRow.content.cloneNode(true);
                    let row = timeElement.parentNode.parentNode;
                    if (!row.previousElementSibling || !row.previousElementSibling.classList.contains("day")) {
                        row.parentNode.insertBefore(instance, row);
                    }
                }
                last_day = dateString;
            }

            // Update the time to shorten it to not include the date
            // (unless that date is significant)
            timeElement.textContent = local.toLocaleTimeString(undefined, {
                timeStyle: 'short'
            });

            if (dateString != last_day && window.location.pathname.includes('requests')) {
                timeElement.textContent = (new Date(local)).toLocaleDateString(undefined, {}) + " " + timeElement.textContent;
            }
        });

        // Craft the 'up-next' schedule
        const urlParams = new URLSearchParams(window.location.search);
        let currentDate = urlParams.get('now') ? window.luxon.DateTime.fromISO(urlParams.get('now')) : window.luxon.DateTime.now().toLocal();

        // Find which slot this is current on
        let nextTemplate = document.querySelector('#event-next template.next');
        let currentTable = document.querySelector('#event-next table');
        let started = true;
        let until = 0;
        if (currentTable) {
            let lastDate = null;
            let lastElement = null;
            Array.from(document.querySelectorAll('#event-schedule table.slots tr:not(.day) td.time time.start')).some( (timeElement) => {
                const tzDate = timeElement.getAttribute('datetime');
                const startDate = window.luxon.DateTime.fromISO(tzDate).toLocal();
                const endElement = timeElement.closest('.slot').querySelector('time.end');
                let endDate = startDate.plus({hours: 6});
                if (endElement) {
                    const tzEndDate = endElement.getAttribute('datetime');
                    endDate = window.luxon.DateTime.fromISO(tzEndDate);
                }

                if (lastDate === null) {
                    lastDate = startDate;
                    lastElement = timeElement;
                    started = false;

                    // Number of seconds until it starts
                    until = eventStartDate.diff(currentDate).milliseconds / 1000.0;
                }

                // Did this entry happen before our current date?
                if (startDate < currentDate && currentDate > eventStartDate) {
                    lastDate = startDate;
                    lastElement = timeElement;
                    started = true;
                }
                
                // Is this event happening right now? If so, just use THIS (if a jam)
                if (!eventIsRaidTrain && endDate > currentDate) {
                    return true;
                }
                return false;
            });

            // This is the next entry... copy over last and then at least the next 5 slots
            // (or at least one that isn't currently 'on stage')
            if (lastDate) {
                // Reveal the up-next table (if wanted)
                if (!window.location.pathname.includes("events/28")) {
                  currentTable.parentNode.setAttribute('aria-hidden', 'false');
                  currentTable.parentNode.removeAttribute('hidden');
                }

                // Don't do anything if the row is the same as the one that
                // is already there.
                timeElement = lastElement;

                let currentRowTime = currentTable.querySelector('tr:not(.day) time');

                if (currentRowTime && currentRowTime.getAttribute('datetime') == timeElement.getAttribute('datetime')) {
                    if (until > 0 && !forcibly_started) {
                        showStream(forcibly_started_handle, started || forcibly_started, until, null);
                    }
                    else if (countdown.getAttribute('aria-hidden') === 'false') {
                        let row = timeElement.parentNode.parentNode;
                        const span = row.querySelector('span.name');
                        const handle = span.textContent;
                        let nextRow = row.nextElementSibling;
                        let next = null;
                        while (nextRow && !nextRow.classList.contains("slot")) {
                            nextRow = nextRow.nextElementSibling;
                        }
                        if (nextRow) {
                            next = nextRow.querySelector('span.name').textContent;
                        }
                        showStream(handle, started || forcibly_started, until, next);
                    }
                }
                else {
                    currentTable.innerHTML = "";

                    let row = timeElement.parentNode.parentNode;
                    for (var i = 0; i < 20; i++) {
                        let newRow = row.cloneNode(true);
                        newRow.removeAttribute('id');
                        currentTable.appendChild(newRow);
                        const thisRow = row;
                        const span = row.querySelector('span.name');

                        timeElement = row.querySelector('time.start');
                        const tzDate = timeElement.getAttribute('datetime');
                        const startDate = window.luxon.DateTime.fromISO(tzDate).toLocal();
                        const endElement = timeElement.closest('.slot').querySelector('time.end');
                        let endDate = startDate.plus({hours: 6});
                        if (endElement) {
                            const tzEndDate = endElement.getAttribute('datetime');
                            endDate = window.luxon.DateTime.fromISO(tzEndDate);
                        }
                        
                        row = row.nextElementSibling;
                        while (row && row.classList.contains("bio")) {
                            if (row.classList.contains("bio")) {
                                // Also append the bio slot
                                if (nextTemplate) {
                                    let headerRow = nextTemplate.content.cloneNode(true);
                                    let bioButton = row.querySelector('button.open-bio');

                                    if (!bioButton) {
                                        bioButton = row.previousElementSibling.querySelector('button.open-bio');
                                    }

                                    if (bioButton) {
                                        headerRow.querySelector('p').textContent = bioButton.textContent;
                                    }

                                    currentTable.appendChild(headerRow);
                                }

                                let newBioRow = row.cloneNode(true);
                                currentTable.appendChild(newBioRow);
                                row = row.nextElementSibling;

                                let bioButton = newBioRow.querySelector('button');
                                if (!bioButton) {
                                    bioButton = newBioRow.previousElementSibling.querySelector('button.open-bio');
                                    if (!bioButton) {
                                        bioButton = newBioRow.previousElementSibling.previousElementSibling.querySelector('button.open-bio');
                                    }
                                }

                                if (bioButton) {
                                    bioButton.addEventListener('click', openBio);
                                }

                                if (nextTemplate) {
                                    newBioRow.removeAttribute('hidden');
                                    newBioRow.setAttribute('aria-hidden', 'false');
                                    if (bioButton) {
                                        bioButton.remove();
                                    }
                                }
                            }
                        }

                        while (row && row.classList.contains("day")) {
                            row = row.nextElementSibling;
                        }

                        if (i == 0 && nextTemplate) {
                            // Show the current stream, if there is a handle
                            if (span) {
                                const handle = span.textContent;
                                let next = null;
                                let nextRow = thisRow.nextElementSibling;
                                while (nextRow && !nextRow.classList.contains("slot")) {
                                    nextRow = nextRow.nextElementSibling;
                                }
                                if (nextRow) {
                                    next = nextRow.querySelector('span.name').textContent;
                                }
                                showStream(handle, started || forcibly_started, until, next);
                            }

                            const nextHeaderRow = nextTemplate.content.cloneNode(true)
                            currentTable.appendChild(nextHeaderRow);
                            nextTemplate = null;
                        }

                        if (!row) {
                            break;
                        }

                        // Is this either not happening yet or has finished somehow
                        if ((startDate > currentDate || endDate < currentDate) && i > 5) {
                            // We can stop now... we have the next person 'up to bat'
                            break;
                        }
                    }
                    updateStreamNavigation();
                }
            }
        }
    }

    window.setInterval(update_schedule, 1000);

    /**
     * This updates the current streaming list near the active stream.
     */
    function updateStreamNavigation() {
        // Get all service ids from the current panel
        let streamNav = document.querySelector('#presentation ul.streams');
        if (!streamNav) {
            return;
        }

        let streamTemplate = document.querySelector('#presentation template.stream');
        if (!streamTemplate) {
            return;
        }

        let formData = new FormData();
        let tag = Math.random();
        document.querySelectorAll('#event-next.panel table.slots tr.slot td.link span.name:not(.collaborator)').forEach( (link) => {
            streamItem = streamNav.querySelector(`li.stream[data-service-handle="${link.textContent}"]`);
            if (!streamItem) {
                // Create navigation button (if it doesn't already exist)
                streamItem = streamTemplate.content.cloneNode(true).querySelector('li');
                streamItem.setAttribute('data-service-handle', link.textContent);
                streamItem.querySelector('img.avatar').src = link.parentNode.parentNode.previousElementSibling.querySelector('img.avatar').src;
                streamItem.querySelector('span.handle').textContent = link.textContent;
                streamItem.querySelector('span.streaming').textContent = '';
                streamItem.setAttribute('data-service-id', link.getAttribute('data-service-id'));
                streamNav.appendChild(streamItem);

                // Attach event
                let button = streamItem.querySelector('button');
                if (button) {
                    button.addEventListener('click', (event) => {
                        // Show this stream
                        forcibly_started_handle = link.textContent;
                        forcibly_started = true;
                        showStream(link.textContent, true, 0, null);
                    });
                }
            }
            streamItem.setAttribute('data-tag', tag);

            // Retain id
            let serviceId = link.getAttribute('data-service-id');
            if (serviceId) {
                formData.append('ids[]', serviceId);
            }
        });

        // Remove any stale ones
        document.querySelectorAll(`#presentation li.stream:not([data-tag="${tag}"])`).forEach( (streamItem) => {
            streamItem.remove();
        });

        // Ask the backend which of the set of broadcaster ids are live and get
        // info about them.
        const apiURL = '/api/twitch/info';
        fetch(apiURL, {
            method: 'POST',
            body: formData,
        }).then( (response) => {
            return response.json();
        }).then( (data) => {
            Object.keys(data).forEach( (id) => {
                item = data[id];
                // item.title is the title of the stream
                // item.tags is an array of tags
                // item.live is a boolean for whether or not they are active
                streamNav.querySelectorAll(`li.stream[data-service-id="${id}"]`).forEach( (streamItem) => {
                    streamItem.querySelector('span.streaming').textContent = (
                        item.live ? item.game_name : ''
                    );
                    streamItem.querySelector('.live-sticker').setAttribute('data-status',
                        item.live ? 'live' : 'offline'
                    );

                    if (!item.live) {
                        streamItem.setAttribute('hidden', '');
                        streamItem.setAttribute('aria-hidden', 'true');
                    }
                    else {
                        streamItem.removeAttribute('hidden');
                        streamItem.setAttribute('aria-hidden', 'false');
                    }
                });

                // Look for calendar items for this stream id and make them not current if not live
                document.querySelectorAll(`.calendar-list-item span.name[data-service-id="${id}"]`).forEach( (calendarSlot) => {
                  const itemElement = calendarSlot.closest('.calendar-list-item');
                  if (item.live) {
                    itemElement.classList.add('current')
                  }
                  else {
                    itemElement.classList.remove('current')
                  }
                  itemElement.classList.add('set-by-navigation')
                });
            });
        });
    }

    window.setInterval(updateStreamNavigation, 30000);
    updateStreamNavigation();

    function showStream(handle, started, until, next) {
        const countdown = document.querySelector('#countdown');
        if (started) {
            updateStreamNavigation();

            let streamNav = document.querySelector('#presentation ul.streams');
            if (streamNav) {
                let streamItem = streamNav.querySelector(`li.stream[data-service-handle="${handle}"]`);
                if (streamItem) {
                    // Reorder button
                    streamItem.parentNode.insertBefore(streamItem, streamItem.parentNode.firstChild);
                }
            }

            const panel = document.querySelector('#twitch-embed').parentNode;
            try {
                document.querySelector('#twitch-embed').innerHTML = '';
                panel.removeAttribute('hidden');
                panel.setAttribute('aria-hidden', 'false');
                countdown.setAttribute('hidden', '');
                countdown.setAttribute('aria-hidden', 'true');
                embed = new Twitch.Embed("twitch-embed", {
                    width: '100%',
                    height: 480,
                    channel: handle,
                });
                currentlyStreaming.handle = handle;
                currentlyStreaming.live = true;

                embed.addEventListener(Twitch.Embed.VIDEO_READY, function() {
                    console.log(embed.getEnded());
                });

                embed.addEventListener(Twitch.Player.OFFLINE, function() {
                    currentlyStreaming.live = false;
                    if (next) {
                        showStream(next, started, until, null);
                    }
                    else {
                        // Find a live stream from the embed nav, if all else fails
                        const streamNav = document.querySelector('#presentation ul.streams');
                        const live = streamNav.querySelector('.live-sticker[data-status="live"]');

                        if (live) {
                            const streamItem = live.closest('li.stream');
                            const liveHandle = streamItem.querySelector('span.handle').textContent;
                            if (liveHandle !== handle) {
                                showStream(liveHandle, started, until, null);
                            }
                        }
                    }
                });
            }
            catch (e) {
                console.log("error", e);
                panel.setAttribute('hidden', '');
                panel.setAttribute('aria-hidden', 'true');
            }
        }
        else {
            const panel = document.querySelector('#twitch-embed').parentNode;
            panel.removeAttribute('hidden');
            panel.setAttribute('aria-hidden', 'false');
            const twitchEmbed = document.querySelector('#twitch-embed');
            if (twitchEmbed) {
                twitchEmbed.innerHTML = '';
            }
            countdown.removeAttribute('hidden');
            countdown.setAttribute('aria-hidden', 'false');
            let seconds = Math.floor(until % 60);
            until /= 60;
            let minutes = Math.floor(until % 60);
            until /= 60;
            let hours = Math.floor(until % 24);
            until /= 24;
            let days = Math.floor(until);
            let text = '';
            if (days > 0) {
                countdown.classList.add("has-days");
                hours = ('' + hours).padStart(2, '0');
                text = days + ":";
            }
            if (days > 0 || hours > 0) {
                countdown.classList.add("has-hours");
                minutes = ('' + minutes).padStart(2, '0');
                text += hours + ":";
            }
            if (days > 0 || hours > 0 || minutes > 0) {
                countdown.classList.add("has-minutes");
                seconds = ('' + seconds).padStart(2, '0');
                text += minutes + ":";
            }
            text += seconds;
            countdown.innerText = text;
        }
    }

    // Do the raid message copy
    const raidMessage = document.querySelector('p.copy-raid-message');
    if (navigator && navigator.clipboard && raidMessage) {
        const raidMessageCopyLink = document.querySelector('a.copy-raid-message');
        if (raidMessageCopyLink) {
            raidMessageCopyLink.addEventListener('click', (event) => {
                event.stopPropagation();
                event.preventDefault();
                navigator.clipboard.writeText(
                    raidMessage.querySelector('span.message').textContent
                ).then( () => {
                    const copied = raidMessage.querySelector('span.copied');
                    if (copied) {
                        copied.removeAttribute('hidden');
                        copied.setAttribute('aria-hidden', 'false');
                        setTimeout( () => {
                            copied.setAttribute('hidden', '');
                            copied.setAttribute('aria-hidden', 'true');
                        }, 2000);
                    }
                });
            });
            raidMessage.removeAttribute('hidden');
            raidMessage.setAttribute('aria-hidden', 'false');
        }
    }
})();
