(() => {
    // Detect if times are rendered with timezone
    let date = new Date("2023-11-12T15:30:00Z");
    console.log(date.toTimeString());
    var offset = date.toTimeString().split('GMT')[1].split(' ')[0];
    console.log(offset);
    let minutes = parseInt(offset.substring(offset.length - 2)) + (parseInt(offset.substring(0, offset.length - 2)) * 60);
    console.log(minutes);
    let datestr = date.toLocaleString(undefined, {
      timeStyle: 'short',
    });
    console.log(datestr);
    let rendersAsUTC = datestr.indexOf("3:30") > -1;
    let utcDifference = 0;
    let wouldBeDifference = minutes * 60000;
    wouldBeDifference = window.luxon.DateTime.local().offset * 60000;
    if (rendersAsUTC) {
      utcDifference = wouldBeDifference;
    }

    // Remove timezone label from form so it says times are in local timezone
    const tzLabel = document.querySelector('p.timezone-label:last-child');
    if (tzLabel) {
        tzLabel.remove();
        tzLabel.removeAttribute('hidden');
        tzLabel.setAttribute('aria-hidden', 'false');
    }

    // Set the timezone offset
    document.querySelector('input[type="hidden"][name="offset"]').value = wouldBeDifference;

    // Update the time inputs to be their offsets
    document.querySelectorAll('input[type="datetime-local"]').forEach( (inputTimeElement) => {
        if (inputTimeElement.value) {
            let tzDate = inputTimeElement.value + 'Z';
            const newDate = (new Date((new Date(inputTimeElement.value)).valueOf() + wouldBeDifference * 2));
            const dateString = window.luxon.DateTime.fromISO(tzDate).toLocal().toISO();
            console.log(inputTimeElement.value);
            console.log((new Date(inputTimeElement.value)));
            console.log(wouldBeDifference);
            console.log(newDate);
            console.log(window.luxon.DateTime.fromJSDate(new Date(inputTimeElement.value)).toUTC().toISO());
            console.log(window.luxon.DateTime.fromISO(tzDate).toLocal().toISO());
            //const dateString = newDate.toISOString();
            console.log(dateString);
            let snipPoint = dateString.indexOf('.');
            if (snipPoint < 0) {
                snipPoint = dateString.indexOf('Z');
            }
            if (snipPoint < 0) {
                snipPoint = dateString.length;
            }
            const inputString = dateString.substring(0, snipPoint);
            console.log(inputString);
            inputTimeElement.value = inputString;
        }
    });
})();
