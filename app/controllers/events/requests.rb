# frozen_string_literal: true

# RaidTrainer - Collaborative Stream Event Planning and Presentation
# Copyright (C) 2023 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group Event Routes
class RaidTrainer
  module Controllers
    # This represents all routes that act upon a Request.
    class RequestController < RaidTrainer::Controller
      before '/events/:event_id/requests*' do
        @event = Event.find(params[:event_id])

        params[:code] ||= session[:code]
        session[:code] = params[:code]

        halt(404) if params[:code] != @event.code && !@event.owned_by?(current_actor)
      end

      # List all requests of a particular event
      get '/events/:event_id/requests' do
        render :slim, :'events/requests/index', locals: {
          event: @event,
          background: @event.url_for_background,
        }
      end

      # Show the form to create a request
      get '/events/:event_id/requests/new' do
        render :slim, :'events/requests/new', locals: {
          event: @event,
          request: Request.new(
            start: DateTime.now,
            end: DateTime.now
          ),
          background: @event.url_for_background,
        }
      end

      # Create a new request for an event
      post '/events/:event_id/requests' do
        redirect to("/events/#{@event.id}/requests") and return unless @event.open

        # Account for timezone offset
        offset = (params[:offset] || '0').to_i
        offset /= 1000

        if params[:start] && params[:end]
          start = DateTime.parse(params[:start]) - offset.seconds
          params[:start] = start

          if params[:hide_end] == "on"
            params[:end] = params[:start] + 6.hours
            params[:end] = [params[:end], @event.end].min
          else
            final = DateTime.parse(params[:end]) - offset.seconds
            params[:end] = final
          end
        end

        request = Request.new(
          actor: current_actor,
          event: @event,
          start: params[:start],
          end: params[:end],
          note: params[:note],
          hide_end: params[:hide_end],
          description: params[:description]
        )
        request.save!

        redirect to("/events/#{@event.id}/requests")
      rescue ActiveRecord::RecordInvalid => e
        session[:flash] = e.message
        render :slim, :'events/requests/new', locals: {
          event: @event,
          request:,
          background: @event.url_for_background,
        }
      end

      # Delete a request
      delete '/events/:event_id/requests/:request_id' do
        request = Request.find(params[:request_id])
        halt(404) if request.actor != current_actor && !@event.owned_by?(current_actor)

        request.destroy!

        session[:flash] = I18n.t('flash.request-deleted')

        redirect to("/events/#{@event.id}/requests")
      end
    end
  end

  use Controllers::RequestController
end
# @!endgroup
