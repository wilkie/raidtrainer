# frozen_string_literal: true

# RaidTrainer - Collaborative Stream Event Planning and Presentation
# Copyright (C) 2023 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group Event Routes
class RaidTrainer
  module Controllers
    # This represents all routes that act upon a Page.
    class PageController < RaidTrainer::Controller
      before '/events/:event_id/*' do
        @event = Event.find(params[:event_id])
      end

      # Render the page
      get '/events/:event_id/pages/:page_id' do
        page = Page.find(params[:page_id])

        halt(404) if page.event != @event

        render :slim, :'events/pages/show', locals: {
          event: @event,
          page:,
          background: @event.url_for_background,
        }
      end
    end
  end

  use Controllers::PageController
end
# @!endgroup
