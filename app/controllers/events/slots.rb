# frozen_string_literal: true

# RaidTrainer - Collaborative Stream Event Planning and Presentation
# Copyright (C) 2023 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group Event Routes
class RaidTrainer
  module Controllers
    # This represents all routes that act upon a Slot.
    class SlotController < RaidTrainer::Controller
      before '/events/:event_id/slots*' do
        @event = Event.find(params[:event_id])
      end

      # List all slots of a particular event
      get '/events/:event_id/slots' do
        halt(404) unless @event.owned_by?(current_actor)

        render :slim, :'events/slots/index', locals: {
          event: @event,
          background: @event.url_for_background,
        }
      end

      # Show the form to create a slot
      get '/events/:event_id/slots/new' do
        halt(404) unless @event.owned_by?(current_actor)

        request = Request.new
        if params[:request]
          request = Request.find_by(id: params[:request])
        elsif params[:after]
          last_slot = Slot.find_by(id: params[:after])
          if last_slot
            request.start = last_slot.end - 30.minutes
          end

          if last_slot.next
            request.end = last_slot.next.start + 30.minutes
          else
            request.end = request.start + 2.hours
          end
        end

        render :slim, :'events/slots/new', locals: {
          event: @event,
          slot: Slot.new(
            event: @event,
            actor: request.actor,
            start: request.start,
            end: request.end,
            description: request.description
          ),
          background: @event.url_for_background,
        }
      end

      # Create a new slot for an event
      post '/events/:event_id/slots' do
        halt(404) unless @event.owned_by?(current_actor)

        # Account for timezone offset
        offset = (params[:offset] || '0').to_i
        offset /= 1000

        if params[:start] && params[:end]
          start = DateTime.parse(params[:start]) - offset.seconds
          params[:start] = start

          if params[:hide_end] == "on"
            params[:end] = params[:start] + 6.hours
            params[:end] = [params[:end], @event.end].min
          else
            final = DateTime.parse(params[:end]) - offset.seconds
            params[:end] = final
          end
        end

        slot = Slot.new(
          event: @event,
          start: params[:start],
          end: params[:end],
          hide_end: params[:hide_end],
          description: params[:description],
          bio: params[:bio],
          image: params[:image]
        )
        slot.actor = Actor.find_by_downcase_handle(params[:handle].strip)

        if slot.actor.nil?
          # Create the actor
          slot.actor = Actor.create_for! @event.service, params[:handle].strip
        end

        slot.save!

        session[:flash] = I18n.t('flash.slot-created')

        if params[:referer]
          redirect to(params[:referer])
        else
          redirect to("/events/#{@event.id}/slots")
        end
      rescue ActiveRecord::RecordInvalid, Date::Error => e
        session[:flash] = e.message
        render :slim, :'events/slots/new', locals: {
          event: @event,
          slot: slot || Slot.new,
          background: @event.url_for_background,
        }
      end

      # Edit a particular event slot
      get '/events/:event_id/slots/:slot_id/edit' do
        event = Event.find(params[:event_id])
        slot = Slot.find(params[:slot_id])

        halt(404) if slot.event != event
        halt(404) if !slot.owned_by?(current_actor) && !event.owned_by?(current_actor)

        render :slim, :'events/slots/edit', locals: {
          event:,
          slot:,
          background: event.url_for_background,
        }
      end

      # Delete a slot
      delete '/events/:event_id/slots/:slot_id' do
        halt(404) unless @event.owned_by?(current_actor)

        slot = Slot.find(params[:slot_id])
        halt(404) if slot.event != @event
        slot.destroy!

        @redis ||= RedisCache.new
        @redis.del("events-#{@event.id}")

        session[:flash] = I18n.t('flash.slot-deleted')

        redirect to("/events/#{@event.id}?at=#{slot.start.iso8601.gsub('T', ':').gsub('Z', '')}")
      end

      # Update an event slot
      put '/events/:event_id/slots/:slot_id' do
        event = Event.find(params[:event_id])
        slot = Slot.find(params[:slot_id])

        halt(404) if slot.event != event
        halt(404) unless slot.owned_by?(current_actor)

        # Account for timezone offset
        offset = (params[:offset] || '0').to_i
        offset /= 1000

        if params[:start] && params[:end]
          start = DateTime.parse(params[:start]) - offset.seconds
          params[:start] = start

          if params[:hide_end] == "on"
            params[:end] = params[:start] + 6.hours
            params[:end] = [params[:end], @event.end].min
          else
            final = DateTime.parse(params[:end]) - offset.seconds
            params[:end] = final
          end
        end

        if slot.can_edit_time?(current_actor)
          slot.update!(params.select { |k, _| %i[start end image description bio hide_end].include?(k.intern) })
        else
          slot.update!(params.select { |k, _| %i[image description bio].include?(k.intern) })
        end

        @redis ||= RedisCache.new
        @redis.del("events-#{event.id}")

        session[:flash] = I18n.t('flash.saved')
        redirect to("/events/#{event.id}/slots/#{slot.id}/edit")
      rescue ActiveRecord::RecordInvalid => e
        session[:flash] = e.message
        render :slim, :'events/slots/edit', locals: {
          event:,
          slot:,
          background: event.url_for_background,
        }
      end
    end
  end

  use Controllers::SlotController
end
# @!endgroup
