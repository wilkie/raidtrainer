# frozen_string_literal: true

# RaidTrainer - Collaborative Stream Event Planning and Presentation
# Copyright (C) 2023 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group Benefit Routes
class RaidTrainer
  module Controllers
    # This represents all routes that act upon a Benefit.
    class BenefitController < RaidTrainer::Controller
      before '/events/:event_id/benefits*' do
        @event = Event.find(params[:event_id])
      end

      # List all benefits of a particular event
      get '/events/:event_id/benefits' do
        halt(404) unless @event.owned_by?(current_actor)

        render :slim, :'events/benefits/index', locals: {
          event: @event,
          background: @event.url_for_background
        }
      end

      # Show the form to create a benefit for an event
      get '/events/:event_id/benefits/new' do
        halt(404) unless @event.owned_by?(current_actor)

        render :slim, :'events/benefits/edit', locals: {
          event: @event,
          benefit: Benefit.new(
            event: @event
          ),
          background: @event.url_for_background,
        }
      end

      # Poll the information of a given benefit for an event
      get '/events/:event_id/benefits/:benefit_id' do
        benefit = @event.benefits.find(params[:benefit_id])

        content_type 'application/json'
        benefit.poll
      end

      # Create a new benefit for an event
      post '/events/:event_id/benefits' do
        halt(404) unless @event.owned_by?(current_actor)

        benefit = Benefit.create_for!(
          url: params[:url],
          event: @event,
        )

        session[:flash] = I18n.t('flash.benefit-created')

        if params[:referer]
          redirect to(params[:referer])
        else
          redirect to("/events/#{@event.id}/benefits")
        end
      rescue ActiveRecord::RecordInvalid => e
        session[:flash] = e.message
        render :slim, :'events/benefits/new', locals: {
          event: @event,
          benefit: benefit || Benefit.new,
          background: @event.url_for_background,
        }
      end

      # Edit a particular event benefit
      get '/events/:event_id/benefits/:benefit_id/edit' do
        halt(404) unless @event.owned_by?(current_actor)

        benefit = @event.benefits.find(params[:benefit_id])

        render :slim, :'events/benefits/edit', locals: {
          event: @event,
          benefit:,
          background: @event.url_for_background,
        }
      end

      # Delete a benefit
      delete '/events/:event_id/benefits/:benefit_id' do
        halt(404) unless @event.owned_by?(current_actor)

        benefit = @event.benefits.find(params[:benefit_id])
        benefit.destroy!

        @event.invalidate_cache!

        session[:flash] = I18n.t('flash.benefit-deleted')

        redirect to("/events/#{@event.id}/benefits")
      end

      # Update an event benefit
      put '/events/:event_id/benefits/:benefit_id' do
        halt(404) unless @event.owned_by?(current_actor)
        benefit = @event.benefits.find(params[:benefit_id])

        benefit.update!(params.select { |k, _| %i[name description donate_url cause_name cause_description url].include?(k.intern) })
        benefit.update_for_service!

        @event.invalidate_cache!

        session[:flash] = I18n.t('flash.saved')
        redirect to("/events/#{@event.id}/benefits")
      rescue ActiveRecord::RecordInvalid => e
        session[:flash] = e.message
        render :slim, :'events/benefits/edit', locals: {
          event: @event,
          benefit:,
          background: @event.url_for_background,
        }
      end
    end
  end

  use Controllers::BenefitController
end
# @!endgroup
