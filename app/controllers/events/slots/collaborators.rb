# frozen_string_literal: true

# RaidTrainer - Collaborative Stream Event Planning and Presentation
# Copyright (C) 2023 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group Event Routes
class RaidTrainer
  module Controllers
    # This represents all routes that act upon a Collaborator of a Slot.
    class CollaboratorController < RaidTrainer::Controller
      before '/events/:event_id/slots/:slot_id/collaborators*' do
        @event = Event.find(params[:event_id])
        @slot = Slot.find(params[:slot_id])

        halt(404) if @slot.event != @event
        halt(404) if !@slot.owned_by?(current_actor) && !@event.owned_by?(current_actor)
      end

      # List all collaborators of a particular event and slot
      get '/events/:event_id/slots/:slot_id/collaborators' do
        render :slim, :'events/slots/collaborators/index', locals: {
          event: @event,
          background: @event.url_for_background,
        }
      end

      # Show the form to create a slot
      get '/events/:event_id/slots/:slot_id/collaborators/new' do
        collaborator = Collaborator.new(event: @event, slot: @slot)
        render :slim, :'events/slots/collaborators/new', locals: {
          event: @event,
          slot: @slot,
          collaborator:,
          handle: params[:handle]&.strip,
          background: @event.url_for_background,
        }
      end

      # Create a new collaborator for an event slot
      post '/events/:event_id/slots/:slot_id/collaborators' do
        collaborator = Collaborator.create(
          event: @event,
          slot: @slot,
          bio: params[:bio],
          image: params[:image]
        )
        collaborator.actor = Actor.find_by_downcase_handle(params[:handle].strip)

        if collaborator.actor.nil?
          # Create the actor
          collaborator.actor = Actor.create_for! @event.service, params[:handle].strip
        end

        collaborator.save!

        @redis ||= RedisCache.new
        @redis.del("events-#{@event.id}")

        session[:flash] = I18n.t('flash.collaborator-created')

        if params[:referer]
          redirect to(params[:referer])
        else
          redirect to("/events/#{@event.id}/slots/#{@slot.id}/collaborators")
        end
      rescue ActiveRecord::RecordInvalid => e
        session[:flash] = e.message
        render :slim, :'events/slots/collaborators/new', locals: {
          event: @event,
          slot: @slot,
          collaborator: collaborator || Collaborator.new,
          handle: params[:handle]&.strip,
          background: @event.url_for_background,
        }
      end

      # Edit a particular collaborator for the given event slot
      get '/events/:event_id/slots/:slot_id/collaborators/:collaborator_id/edit' do
        collaborator = Collaborator.find(params[:collaborator_id])

        render :slim, :'events/slots/collaborators/edit', locals: {
          event: @event,
          slot: @slot,
          handle: collaborator&.actor&.links&.where(service: @event.service)&.first&.handle,
          collaborator:,
          background: @event.url_for_background,
        }
      end

      # Delete a collaborator
      delete '/events/:event_id/slots/:slot_id/collaborators/:collaborator_id' do
        collaborator = Collaborator.find(params[:collaborator_id])
        collaborator.destroy!

        @redis ||= RedisCache.new
        @redis.del("events-#{@event.id}")

        session[:flash] = I18n.t('flash.collaborator-deleted')

        redirect to("/events/#{@event.id}#slot-#{@slot.id}")
      end

      # Update a collaborator
      put '/events/:event_id/slots/:slot_id/collaborators/:collaborator_id' do
        collaborator = Collaborator.find(params[:collaborator_id])

        collaborator.update!(params.select { |k, _| %i[image bio].include?(k.intern) })

        @redis ||= RedisCache.new
        @redis.del("events-#{@event.id}")

        session[:flash] = I18n.t('flash.saved')
        redirect to("/events/#{@event.id}/slots/#{@slot.id}/collaborators/#{collaborator.id}/edit")
      rescue ActiveRecord::RecordInvalid => e
        session[:flash] = e.message
        render :slim, :'events/slots/collaborators/edit', locals: {
          event: @event,
          slot: @slot,
          handle: params[:handle]&.strip,
          collaborator:,
          background: @event.url_for_background,
        }
      end
    end
  end

  use Controllers::CollaboratorController
end
# @!endgroup
