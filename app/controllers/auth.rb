# frozen_string_literal: true

# RaidTrainer - Collaborative Stream Event Planning and Presentation
# Copyright (C) 2023 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group Event Routes
class RaidTrainer
  module Controllers
    # This represents routes that interact with authentication.
    class AuthController < RaidTrainer::Controller
      # The callback from an authentication
      get '/auth/twitch/callback' do
        # Get the authorization information from
        auth_hash = request.env['omniauth.auth']

        # Get a redirect
        session[:redirect_to] = request.env['omniauth.origin'] || session[:redirect_to]

        # Find the actor logging in
        actor = Actor.find_or_create_from_omniauth!(auth_hash)

        # Log in
        session[:actor] = actor.id

        # Redirect, maybe
        unless session[:redirect_to].blank?
          redirect to(session[:redirect_to])
        else
          # I have to render the index... it won't keep the session if
          # I redirect for some reason. WHY?
          render :slim, :index
        end
      end

      # Log in (show the authentication buttons)
      get '/login' do
        render :slim, :login
      end

      # Log out
      get '/logout' do
        session[:actor] = nil
        session[:code] = nil

        redirect '/'
      end

      get '/auth/failure' do
        session[:flash] = params[:message]
        redirect to('/login')
      end

      post '/auth/local' do
        halt(404) unless RaidTrainer.development?

        if params[:last]
          session[:actor] = Actor.last.id
        else
          session[:actor] = Actor.first.id
        end

        if params[:referer]
          redirect to(params[:referer])
        else
          render :slim, :index
        end
      end
    end
  end

  use Controllers::AuthController
end
# @!endgroup
