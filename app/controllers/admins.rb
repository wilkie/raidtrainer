# frozen_string_literal: true

# RaidTrainer - Collaborative Stream Event Planning and Presentation
# Copyright (C) 2023 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group Admin Routes
class RaidTrainer
  module Controllers
    # This represents all routes that act upon an Event.
    class AdminController < RaidTrainer::Controller
      # Allow an admin to assume an identity
      get '/admins/assume' do
        # Must be logged in
        halt(401) unless logged_in?

        # Authorize
        halt(403) unless current_actor.admin?

        render :slim, :'admins/assume'
      end

      post '/admins/assume' do
        # Must be logged in
        halt(401) unless logged_in?

        # Authorize
        halt(403) unless current_actor.admin?

        # Look up an existing actor via that handle
        actor = Actor.find_by_downcase_handle(params[:handle])

        # Create the Actor if we don't know it yet
        actor = Actor.create_for!('twitch', params[:handle]) unless actor

        # Raise an error if we cannot get the actor at all
        raise StandardError.new('Actor not found') unless actor

        session[:actor] = actor.id
        redirect to('/events')
      rescue StandardError => e
        session[:flash] = e.message
        render :slim, :'admins/assume', locals: {
          handle: params[:handle]
        }
      end
    end
  end

  use Controllers::AdminController
end
# @!endgroup
