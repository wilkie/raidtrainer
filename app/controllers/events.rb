# frozen_string_literal: true

# RaidTrainer - Collaborative Stream Event Planning and Presentation
# Copyright (C) 2023 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group Event Routes
class RaidTrainer
  module Controllers
    # This represents all routes that act upon an Event.
    class EventController < RaidTrainer::Controller
      # List all events
      get '/events' do
        render :slim, :'events/index'
      end

      # Show the form to create an event
      get '/events/new' do
        # Must log in
        halt(400) unless logged_in?

        # Authorize
        halt(403) unless Event.can_create?(current_actor)

        # Render the form
        render :slim, :'events/new', locals: {
          event: Event.new(hidden: true),
        }
      end

      # Create a new event
      post '/events' do
        # Must log in
        halt(400) unless logged_in?

        # Authorize
        halt(403) unless Event.can_create?(current_actor)

        # Create the event
        event = Event.new(
          owner: current_actor
        )

        # Account for timezone offset
        offset = (params[:offset] || '0').to_i
        offset /= 1000

        start = DateTime.parse(params[:start]) - offset.seconds
        final = DateTime.parse(params[:end]) - offset.seconds
        params[:start] = start
        params[:end] = final

        params[:event_type] = params[:event_type].to_i if params[:event_type]

        event.update!(params.select do |k, _|
                        %i[name image background start end open raid_message hidden event_type
                           requests_visible].include?(k.intern)
                      end)
        event.service = 'twitch'
        event.save!

        session[:flash] = I18n.t('flash.saved')

        # Redirect to the new event
        redirect to("/events/#{event.id}")
      rescue StandardError => e
        session[:flash] = e.message
        render :slim, :'events/new', locals: {
          event: event || Event.new(hidden: true),
          background: event.url_for_background,
        }
      end

      # Edit a particular event
      get '/events/:event_id/edit' do
        event = Event.find(params[:event_id])

        halt(404) unless event.owned_by?(current_actor)

        render :slim, :'events/edit', locals: {
          event:,
          background: event.url_for_background,
        }
      end

      # List a particular event
      get '/events/:event_id' do
        event = Event.find(params[:event_id])

        halt(404) if event.hidden && !event.owned_by?(current_actor) && !event.participating_in?(current_actor)

        page = nil
        unless event.participating_in?(current_actor) || event.owned_by?(current_actor)
          page = event.cached_page
        end

        unless page
          page = render :slim, :'events/show', locals: {
            event:,
            background: event.url_for_background,
          }
          unless current_actor
            event.cache_page(page)
          end
        end

        page
      end

      # Show just the particular event calendar
      get '/events/:event_id/calendar' do
        event = Event.find(params[:event_id])

        halt(404) if event.hidden && !event.owned_by?(current_actor) && !event.participating_in?(current_actor)

        render :slim, :'events/calendar', locals: {
          event:,
          background: event.url_for_background,
        }
      end

      # Update an event
      put '/events/:event_id' do
        event = Event.find(params[:event_id])

        halt(404) if event.hidden && !event.owned_by?(current_actor)

        # Account for timezone offset
        offset = (params[:offset] || '0').to_i
        offset /= 1000

        start = DateTime.parse(params[:start]) - offset.seconds
        final = DateTime.parse(params[:end]) - offset.seconds
        params[:start] = start
        params[:end] = final

        event.update!(params.select do |k, _|
                        %i[name image background start end description
                           raid_message open hidden requests_visible
                           show_intermission display_calendar
                           allow_participants_to_edit_time].include?(k.intern)
                      end)
        event.invalidate_cache!

        session[:flash] = I18n.t('flash.saved')
        redirect to("/events/#{event.id}/edit")
      rescue ActiveRecord::RecordInvalid => e
        session[:flash] = e.message
        render :slim, :'events/edit', locals: {
          event:,
          background: event.url_for_background,
        }
      end

      # Get the admin page for editing ownership, etc
      get '/events/:event_id/admin' do
        event = Event.find(params[:event_id])

        halt(404) if event.hidden && !event.owned_by?(current_actor)

        render :slim, :'events/admin', locals: {
          event:,
          background: event.url_for_background,
        }
      end

      # Update the owner
      put '/events/:event_id/owner' do
        # Must be logged in
        halt(401) unless logged_in?

        event = Event.find(params[:event_id])

        # Authorize (only the owner)
        halt(403) unless event.owner_id == current_actor.id

        actor = Actor.find_by_downcase_handle(params[:handle])
        raise StandardError.new('Actor not found') unless actor

        event.update!(owner: actor)
        redirect to("/events")
      rescue StandardError => e
        session[:flash] = e.message
        render :slim, :'events/admin', locals: {
          event:,
          handle: params[:handle]
        }
      end

      # Add a CoOwner to an Event
      post '/events/:event_id/co-owners' do
        # Must be logged in
        halt(401) unless logged_in?

        event = Event.find(params[:event_id])

        # Authorize (only the owner)
        halt(403) unless event.owner_id == current_actor.id

        # Find the actor
        actor = Actor.find_by_downcase_handle(params[:handle])

        # Raise an error if we cannot create the actor
        raise StandardError.new('Actor not found') unless actor

        # Create a CoOwner
        CoOwner.create!(event:, actor:)

        # Redirect back
        redirect to("/events/#{event.id}/admin")
      rescue StandardError => e
        session[:flash] = e.message
        render :slim, :'events/admin', locals: {
          event:,
          co_owner_handle: params[:handle]
        }
      end

      # Remove a CoOwner from the Event
      delete '/events/:event_id/co-owners/:co_owner_id' do
        # Must be logged in
        halt(401) unless logged_in?

        event = Event.find(params[:event_id])

        # Authorize (only the owner)
        halt(403) unless event.owner_id == current_actor.id

        co_owner = CoOwner.find(params[:co_owner_id])

        # Only if the CoOwner applies to the event
        halt(403) unless co_owner.event == event

        # Delete it
        co_owner.destroy!

        # Redirect back
        redirect to("/events/#{event.id}/admin")
      end
    end
  end

  use Controllers::EventController
end
# @!endgroup
