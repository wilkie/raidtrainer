# frozen_string_literal: true

# RaidTrainer - Collaborative Stream Event Planning and Presentation
# Copyright (C) 2023 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group Actor Routes
class RaidTrainer
  module Controllers
    # This represents all routes that act upon an Actor.
    class ActorController < RaidTrainer::Controller
      # List all actors
      get '/actors' do
        render :slim, :'actors/index'
      end

      # Show the form to create an account
      get '/actors/new' do
        render :slim, :'actors/new'
      end

      # List a particular actor's profile
      get '/actors/:id' do
        render :slim, :'actors/show'
      end

      # Creates an Actor
      post '/actors' do
        halt(400) if Actor.count > 0 && !(current_actor&.admin).nil?

        actor = Actor.create!(handle: params[:handle], name: params[:handle])
        Admin.create!(actor: actor) if params[:admin]

        session[:actor] = actor.id
        redirect to('/events')
      end
    end
  end

  use Controllers::ActorController
end
# @!endgroup
