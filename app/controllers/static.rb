# frozen_string_literal: true

# RaidTrainer - Collaborative Stream Event Planning and Presentation
# Copyright (C) 2023 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group Static Routes
class RaidTrainer
  module Controllers
    # This represents all routes that act upon static assets and pages.
    class StaticController < RaidTrainer::Controller
      # Shows the default index page, that is, it redirects to /about.
      get '/' do
        render :slim, :index
      end

      # Renders the index page that shows what Occam is and what it does.
      get '/about' do
        render :slim, :'static/about'
      end

      # Successfully get the 404 page (to check its rendering).
      get '/404' do
        render :slim, :'static/404'
      end

      # Successfully get the 500 page (to check its rendering).
      get '/500' do
        render :slim, :'static/500'
      end
    end
  end

  use Controllers::StaticController
end
# @!endgroup
