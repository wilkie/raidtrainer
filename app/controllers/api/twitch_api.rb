# frozen_string_literal: true

# RaidTrainer - Collaborative Stream Event Planning and Presentation
# Copyright (C) 2023 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group Twitch API Routes
class RaidTrainer
  module Controllers
    # This represents all routes that deal with the Twitch API.
    class TwitchApiController < RaidTrainer::Controller
      # Query a set of handles for their stream statuses.
      post '/api/twitch/info' do
        content_type 'application/json'

        ret = {}
        Twitch.get_channel_info(params[:ids] || []).each do |channel|
          ret[channel['broadcaster_id']] = channel
          ret[channel['broadcaster_id']]['live'] = false
        end

        Twitch.get_stream_info(params[:ids] || []).each do |stream|
          ret[stream['user_id']] ||= {}
          ret[stream['user_id']]['live'] = true
        end

        ret.to_json
      end
    end
  end

  use Controllers::TwitchApiController
end
# @!endgroup
