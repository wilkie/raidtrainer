require "minitest"

require "capybara/cucumber"
require_relative "../../lib/application"

Capybara.app = RaidTrainer

Given(/I am on (.+)/) do |path|
  visit path
end
