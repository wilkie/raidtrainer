require 'capybara'
#require 'capybara/minitest/spec'
require 'selenium-webdriver'

puts "Setting up Capybara"

# Configuration for Capybara
Capybara.register_driver :remote_selenium do |app|
  options = {}
  options[:options] = Selenium::WebDriver::Chrome::Options.new
  options[:options].add_argument("--window-size=1400,1400")
  options[:options].add_argument("--no-sandbox")
  options[:options].add_argument("--disable-dev-shm-usage")

  options[:url] = ENV["SELENIUM_URL"]

  Capybara::Selenium::Driver.new(
    app,
    browser: :remote,
    **options,
  )
end

Capybara.configure do |config|
  #config.server = :puma, { Silent: true }
  config.server = :puma
  config.server_host = 'test'
  config.server_port = 9292
end

Capybara.current_driver = :remote_selenium
Capybara.javascript_driver = :remote_selenium
