# Raid Trainer

This project is a website that allows one to organize a raid train or other event related to streaming.

## Usage

See [USAGE.md](USAGE.md) for sharable information about using the website.

## Deploying

See [DEPLOYING.md](DEPLOYING.md) for information about deploying the website.

## Development

The remainder of this document will discuss how to spin up a development version
of the site and ultimately contribute to the development of the project.

It is generally useful to spin up a development environment using the provided
`docker-compose` files:

```shell
FIXUID=$(id -u) docker compose build web web-dev selenium-video
```

This will build a Docker container that contains everything needed to run the server
with an internal user that matches the one you are using to build the container.

If you run the `web` package, this runs the code that is baked into the production
container. You would need to rebuild this container with every source change.

Instead we will use the `web-dev` container which mounts in your source directory.
First, we can use this command to install our ruby libraries to our local space:

```shell
docker compose run web-dev-install
```

You can run this any time libraries change or we need to reconfigure anything. It
will install the libraries and migrate the database.

Then, to spin up the `web-dev` server itself:

```shell
docker compose up web-dev
```

When this service starts, you can navigate to your new instance via:
[http://localhost:9292](http://localhost:9292).

At first, there will be no users. The first user that is created will be an admin.
So, go to 'Sign in' and you will see a form to create your admin user. Just do that
and you'll be good to go.

## Contributing

## Thanks

This project was initially developed for the Twitch Women's Unity Guild. It was used
for events by many of the Unity Guilds including the Twitch Black Unity Guild. They
tested and gave feedback that improved the design.
