include:
  - path: ./docker-compose.networks.yml

services:
  # A Selenium grid hub that will run *-node services that are described below
  selenium-grid:
    image: selenium/hub:${SELENIUM_HUB_VERSION}
    hostname: selenium
    expose:
      # The Selenium 'Publish' port
      - "4442:4442"
      # The Selenium 'Subscribe' port
      - "4443:4443"
      # The Protocol port that our testing infrastructure connects to
      - "4444:4444"  
    environment: &selenium-grid-environment
      SE_EVENT_BUS_HOST: selenium
      SE_EVENT_BUS_PUBLISH_PORT: 4442
      SE_EVENT_BUS_SUBSCRIBE_PORT: 4443
      SE_AVOID_STATS: 'true'
    ulimits: &selenium-ulimits
      nofile:
        soft: 32768
        hard: 32768
    networks:
      raidtrainer_network:

  selenium-video:
    hostname: selenium-video
    image: codedotorg/selenium-video
    build:
      dockerfile: ./selenium-video.Dockerfile
      context: ./selenium-video
    environment:
      DISPLAY_CONTAINER_NAME: raidtrainer-selenium-chrome
    volumes:
      - /tmp:/videos
    networks:
      raidtrainer_network:

  # A Selenium server running Chrome for running UI tests contained in Docker
  selenium-chrome-base: &selenium-chrome-base
    hostname: selenium
    image: seleniarm/standalone-chromium:${SELENIUM_CHROME_VERSION}
    environment:
      JAVA_OPTS: -Dwebdriver.chrome.whitelistedIps= -Dwebdriver.chrome.allowedIps=
      SE_AVOID_STATS: 'true'
    shm_size: '2gb'
    expose: &selenium-exposed-ports
      - "5900" # The selenium direct VNC port
      - "4444" # The selenium router port
      - "7900" # The selenium NoVNC service port
    # Many default ulimits are just too high
    # And, so, it will consume way too much memory (16GB+)
    ulimits: *selenium-ulimits

  selenium-chrome-contained:
    <<: *selenium-chrome-base
    networks:
      raidtrainer_network:

  # A Selenium server running Chrome for running UI tests locally
  selenium-chrome:
    <<: *selenium-chrome-base
    network_mode: host
    ports:
      - "5910:5900" # The selenium direct VNC port
      - "4444:4444" # The selenium router port
      - "7910:7900" # The selenium NoVNC service port

  # A Selenium server that connects through the selenium grid
  selenium-chrome-node:
    # We need an explicit container name for the target of the video service
    container_name: raidtrainer-selenium-chrome
    hostname: chrome
    image: selenium/node-chrome:${SELENIUM_CHROME_VERSION}
    shm_size: 2gb
    depends_on:
      selenium-grid:
        condition: service_started
    environment:
      <<: *selenium-grid-environment
      JAVA_OPTS: -Dwebdriver.chrome.whitelistedIps= -Dwebdriver.chrome.allowedIps=
    expose: *selenium-exposed-ports
    ports:
      - "5910:5900"
      - "7910:7900"
    ulimits: *selenium-ulimits
    networks:
      raidtrainer_network:

  # A Selenium server running Firefox for running UI tests contained in Docker
  selenium-firefox-base: &selenium-firefox-base
    hostname: selenium
    image: seleniarm/standalone-firefox:${SELENIUM_FIREFOX_VERSION}
    shm_size: '2gb'
    expose: *selenium-exposed-ports
    environment:
      SE_AVOID_STATS: 'true'
    ports:
      - "5911:5900" # The selenium direct VNC port
      - "7911:7900" # The selenium NoVNC service port
    ulimits: *selenium-ulimits

  selenium-firefox-contained:
    <<: *selenium-firefox-base
    networks:
      raidtrainer_network:

  # A Selenium server running Firefox for running UI tests locally
  selenium-firefox:
    <<: *selenium-firefox-base
    ports:
      - "5911:5900" # The selenium direct VNC port
      - "4444:4444" # The selenium router port
      - "7911:7900" # The selenium NoVNC service port
    network_mode: host

  # A Selenium server that connects through the selenium grid
  selenium-firefox-node:
    # We need an explicit container name for the target of the video service
    container_name: raidtrainer-selenium-firefox
    hostname: firefox
    image: seleniarm/node-firefox:${SELENIUM_FIREFOX_VERSION}
    shm_size: 2gb
    depends_on:
      selenium-grid:
        condition: service_started
    environment:
      <<: *selenium-grid-environment
    ports:
      - "5911:5900"
      - "7911:7900"
    expose: *selenium-exposed-ports
    ulimits: *selenium-ulimits
    networks:
      raidtrainer_network:

  # A Selenium server running Edge for running UI tests contained in Docker
  selenium-edge-base: &selenium-edge-base
    hostname: selenium
    image: selenium/standalone-edge:${SELENIUM_EDGE_VERSION}
    expose: *selenium-exposed-ports
    environment:
      SE_AVOID_STATS: 'true'
    ports:
      - "5912:5900"
      - "7912:7900"
    shm_size: '2gb'
    ulimits: *selenium-ulimits

  selenium-edge-contained:
    <<: *selenium-edge-base
    networks:
      raidtrainer_network:

  # A Selenium server running Firefox for running UI tests locally
  selenium-edge:
    <<: *selenium-edge-base
    ports:
      - "5912:5900" # The selenium direct VNC port
      - "4444:4444" # The selenium router port
      - "7912:7900" # The selenium NoVNC service port
    network_mode: host

  # A Selenium server that connects through the selenium grid
  selenium-edge-node:
    # We need an explicit container name for the target of the video service
    container_name: raidtrainer-selenium-edge
    hostname: edge
    image: selenium/node-edge:${SELENIUM_EDGE_VERSION}
    shm_size: 2gb
    depends_on:
      selenium-grid:
        condition: service_started
    environment:
      <<: *selenium-grid-environment
    ports:
      - "5912:5900"
      - "7912:7900"
    expose: *selenium-exposed-ports
    ulimits: *selenium-ulimits
    networks:
      raidtrainer_network:
