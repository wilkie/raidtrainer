# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2024_02_24_003930) do
  create_table "actors", force: :cascade do |t|
    t.string "handle"
    t.string "name"
    t.string "avatar_url"
    t.string "timezone"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["handle"], name: "index_actors_on_handle"
    t.index ["name"], name: "index_actors_on_name"
  end

  create_table "admins", force: :cascade do |t|
    t.integer "actor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["actor_id"], name: "index_admins_on_actor_id"
  end

  create_table "authentications", force: :cascade do |t|
    t.integer "actor_id"
    t.string "token"
    t.string "refresh_token"
    t.string "hashed_email"
    t.string "service"
    t.datetime "expires"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["actor_id", "service"], name: "index_authentications_on_actor_id_and_service"
    t.index ["actor_id"], name: "index_authentications_on_actor_id"
  end

  create_table "benefits", force: :cascade do |t|
    t.integer "event_id"
    t.integer "actor_id"
    t.integer "slot_id"
    t.integer "team_id"
    t.string "type", null: false
    t.string "name"
    t.string "cause_id"
    t.string "cause_name"
    t.text "cause_description"
    t.string "supporting"
    t.string "url"
    t.string "service"
    t.string "service_id"
    t.string "service_slug"
    t.string "currency"
    t.integer "goal"
    t.integer "raised"
    t.string "donate_url"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_url"
    t.index ["actor_id"], name: "index_benefits_on_actor_id"
    t.index ["event_id"], name: "index_benefits_on_event_id"
    t.index ["slot_id"], name: "index_benefits_on_slot_id"
    t.index ["team_id"], name: "index_benefits_on_team_id"
  end

  create_table "co_owners", force: :cascade do |t|
    t.integer "actor_id"
    t.integer "event_id"
    t.index ["actor_id"], name: "index_co_owners_on_actor_id"
    t.index ["event_id"], name: "index_co_owners_on_event_id"
  end

  create_table "collaborators", force: :cascade do |t|
    t.integer "actor_id"
    t.integer "slot_id"
    t.string "image"
    t.text "bio"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["actor_id"], name: "index_collaborators_on_actor_id"
    t.index ["slot_id"], name: "index_collaborators_on_slot_id"
  end

  create_table "events", force: :cascade do |t|
    t.integer "owner_id"
    t.integer "co_owner_id"
    t.string "code"
    t.string "name"
    t.string "url"
    t.string "image"
    t.string "background"
    t.string "service"
    t.text "description"
    t.boolean "open"
    t.boolean "hidden"
    t.boolean "requests_visible"
    t.datetime "start"
    t.datetime "end"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "raid_message"
    t.boolean "allow_participants_to_edit_time", default: false
    t.integer "event_type", default: 0
    t.boolean "show_intermission"
    t.boolean "display_calendar", default: false
    t.index ["co_owner_id"], name: "index_events_on_co_owner_id"
    t.index ["name"], name: "index_events_on_name"
    t.index ["owner_id"], name: "index_events_on_owner_id"
    t.index ["service"], name: "index_events_on_service"
  end

  create_table "links", force: :cascade do |t|
    t.integer "actor_id"
    t.string "handle"
    t.string "name"
    t.string "service"
    t.string "avatar_url"
    t.string "url"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "type", null: false
    t.string "service_id"
    t.index ["actor_id"], name: "index_links_on_actor_id"
    t.index ["handle"], name: "index_links_on_handle"
    t.index ["service"], name: "index_links_on_service"
  end

  create_table "pages", force: :cascade do |t|
    t.integer "event_id"
    t.string "name"
    t.string "page_type"
    t.integer "position"
    t.text "content"
    t.string "content_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index "\"type\"", name: "index_pages_on_type"
    t.index ["event_id"], name: "index_pages_on_event_id"
  end

  create_table "requests", force: :cascade do |t|
    t.integer "actor_id"
    t.integer "event_id"
    t.datetime "start"
    t.datetime "end"
    t.text "note"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "hide_end"
    t.index ["actor_id"], name: "index_requests_on_actor_id"
    t.index ["event_id"], name: "index_requests_on_event_id"
  end

  create_table "slots", force: :cascade do |t|
    t.integer "actor_id"
    t.integer "event_id"
    t.string "image"
    t.text "description"
    t.text "bio"
    t.datetime "start"
    t.datetime "end"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "hide_end"
    t.index ["actor_id"], name: "index_slots_on_actor_id"
    t.index ["end"], name: "index_slots_on_end"
    t.index ["event_id"], name: "index_slots_on_event_id"
    t.index ["start"], name: "index_slots_on_start"
  end

  add_foreign_key "admins", "actors"
  add_foreign_key "authentications", "actors"
  add_foreign_key "benefits", "actors"
  add_foreign_key "benefits", "actors", column: "team_id"
  add_foreign_key "benefits", "events"
  add_foreign_key "benefits", "slots"
  add_foreign_key "co_owners", "actors"
  add_foreign_key "co_owners", "events"
  add_foreign_key "collaborators", "actors"
  add_foreign_key "collaborators", "slots"
  add_foreign_key "events", "actors", column: "co_owner_id"
  add_foreign_key "events", "actors", column: "owner_id"
  add_foreign_key "links", "actors"
  add_foreign_key "pages", "events"
  add_foreign_key "requests", "actors"
  add_foreign_key "requests", "events"
  add_foreign_key "slots", "actors"
  add_foreign_key "slots", "events"
end
