class CreateCoownersTable < ActiveRecord::Migration[7.1]
  def change
    create_table :co_owners do |t|
      t.references :actor, index: true, foreign_key: true
      t.references :event, index: true, foreign_key: true
    end
  end
end
