class CreateBenefitsTable < ActiveRecord::Migration[7.1]
  def change
    create_table :benefits do |t|
      t.references :event, index: true, foreign_key: true
      t.references :actor, index: true, foreign_key: true
      t.references :slot, index: true, foreign_key: true
      t.references :team, index: true, foreign_key: {to_table: :actors}
      t.string :type, null: false
      t.string :name
      t.string :cause_id
      t.string :cause_name
      t.text :cause_description
      t.string :supporting
      t.string :url
      t.string :service
      t.string :service_id
      t.string :service_slug
      t.string :currency
      t.integer :goal
      t.integer :raised
      t.string :donate_url
      t.text :description

      t.timestamps
    end
  end
end
