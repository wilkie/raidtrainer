class AddShowIntermissionFlagToEvents < ActiveRecord::Migration[7.1]
  def change
    add_column :events, :show_intermission, :boolean
  end
end
