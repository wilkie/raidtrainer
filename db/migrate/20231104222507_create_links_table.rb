class CreateLinksTable < ActiveRecord::Migration[7.1]
  def change
    create_table :links do |t|
      t.references :actor, index: true, foreign_key: true
      t.string :handle
      t.string :name
      t.string :service
      t.string :avatar_url
      t.string :url
      t.text :description

      t.index [:handle]
      t.index [:service]

      t.timestamps
    end
  end
end
