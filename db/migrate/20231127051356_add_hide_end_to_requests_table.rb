class AddHideEndToRequestsTable < ActiveRecord::Migration[7.1]
  def change
    add_column :requests, :hide_end, :boolean
  end
end
