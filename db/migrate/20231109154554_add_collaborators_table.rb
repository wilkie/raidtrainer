class AddCollaboratorsTable < ActiveRecord::Migration[7.1]
  def change
    create_table :collaborators do |t|
      t.references :actor, index: true, foreign_key: true
      t.references :slot, index: true, foreign_key: true

      t.string :image

      t.text :bio

      t.timestamps
    end
  end
end
