class AddAllowTimeEditToEvents < ActiveRecord::Migration[7.1]
  def change
    add_column :events, :allow_participants_to_edit_time, :boolean, default: false
  end
end
