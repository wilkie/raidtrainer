class AddRaidMessageToEventsTable < ActiveRecord::Migration[7.1]
  def change
    add_column :events, :raid_message, :text
  end
end
