class AddDisplayCalendarToEvents < ActiveRecord::Migration[7.1]
  def change
    add_column :events, :display_calendar, :boolean, default: false
  end
end
