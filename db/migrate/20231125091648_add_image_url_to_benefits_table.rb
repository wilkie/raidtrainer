class AddImageUrlToBenefitsTable < ActiveRecord::Migration[7.1]
  def change
    add_column :benefits, :image_url, :string
  end
end
