class CreatePagesTable < ActiveRecord::Migration[7.1]
  def change
    create_table :pages do |t|
      t.references :event, index: true, foreign_key: true

      t.string :name
      t.string :page_type
      t.integer :position
      t.text :content
      t.string :content_type

      t.index [:type]

      t.timestamps
    end
  end
end
