class AddTypeToLinks < ActiveRecord::Migration[7.1]
  def up
    # Add the type to the Link model, allowing null
    add_column :links, :type, :string

    # Add all existing links to the 'Links::Twitch' type
    execute "UPDATE links SET type = 'Links::Twitch';"

    # Update to ensure that the type is NOT NULL
    change_column :links, :type, :string, null: false
  end

  def down
    remove_column :links, :type
  end
end
