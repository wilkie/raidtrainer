class AddHideEndToSlotsTable < ActiveRecord::Migration[7.1]
  def change
    add_column :slots, :hide_end, :boolean
  end
end
