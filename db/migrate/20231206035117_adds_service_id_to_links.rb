class AddsServiceIdToLinks < ActiveRecord::Migration[7.1]
  def up
    add_column :links, :service_id, :string

    # Re-query for service id
    require_relative '../../lib/application.rb'

    print "Adding service_id to Link "
    count = 0
    Link.all.each do |link|
      count += 1
      print '.' if (count % 10) == 0
      next unless link.service_id.blank?
      info = Twitch::retrieve_user_info_by_handle(link.handle)
      link.update!(service_id: info['id']) if info
    end
    puts
    puts "Done."
  end

  def down
    remove_column :links, :service_id
  end
end
