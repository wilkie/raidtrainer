class CreateAdminsTable < ActiveRecord::Migration[7.1]
  def change
    create_table :admins do |t|
      t.references :actor, index: true, foreign_key: true
      t.timestamps
    end
  end
end
