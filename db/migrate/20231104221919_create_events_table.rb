class CreateEventsTable < ActiveRecord::Migration[7.1]
  def change
    create_table :events do |t|
      t.references :owner, index: true, foreign_key: {to_table: :actors}
      t.references :co_owner, index: true, foreign_key: {to_table: :actors}

      t.string :code
      t.string :name
      t.string :url
      t.string :image
      t.string :background
      t.string :service
      t.text :description

      t.boolean :open
      t.boolean :hidden
      t.boolean :requests_visible

      t.datetime :start
      t.datetime :end

      t.index [:name]
      t.index [:service]

      t.timestamps
    end
  end
end
