class CreateSlotsTable < ActiveRecord::Migration[7.1]
  def change
    create_table :slots do |t|
      t.references :actor, index: true, foreign_key: true
      t.references :event, index: true, foreign_key: true

      t.string :image

      t.text :description
      t.text :bio

      t.datetime :start
      t.datetime :end

      t.index [:start]
      t.index [:end]

      t.timestamps
    end
  end
end
