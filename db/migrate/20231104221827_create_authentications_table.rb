class CreateAuthenticationsTable < ActiveRecord::Migration[7.1]
  def change
    create_table :authentications do |t|
      t.references :actor, index: true, foreign_key: true
      t.string :token
      t.string :refresh_token
      t.string :hashed_email
      t.string :service
      t.datetime :expires

      t.index [:actor_id, :service]

      t.timestamps
    end
  end
end
