class CreateRequestsTable < ActiveRecord::Migration[7.1]
  def change
    create_table :requests do |t|
      t.references :actor, index: true, foreign_key: true
      t.references :event, index: true, foreign_key: true

      t.datetime :start
      t.datetime :end

      t.text :note
      t.text :description

      t.timestamps
    end
  end
end
