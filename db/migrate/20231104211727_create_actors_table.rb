class CreateActorsTable < ActiveRecord::Migration[7.1]
  def change
    create_table :actors do |t|
      t.string :handle
      t.string :name
      t.string :avatar_url
      t.string :timezone
      t.text :description

      t.index [:handle]
      t.index [:name]

      t.timestamps
    end
  end
end
