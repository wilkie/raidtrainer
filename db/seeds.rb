require_relative '../lib/application'

if RaidTrainer.development? and Actor.count == 0
  # Create an Admin account
  puts "Creating admin account named 'admin'"
  Actor.create!(name: 'admin', handle: 'admin')
  Admin.create!(actor: Actor.first)

  # TODO: Create the test event
end
