require "minitest/autorun"
require "minitest/spec"
require 'minitest/around/spec'
require 'mocha/minitest'

Minitest::Spec::DSL.class_eval do
  def configure(&block)
    around do |test|
      RaidTrainer.stub(:configuration, instance_eval(&block)) do
        test.call
      end
    end
  end
end
