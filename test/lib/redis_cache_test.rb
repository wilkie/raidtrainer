require_relative "../test_helper"

require "redis"
require_relative "../../lib/redis_cache"

class RaidTrainer
  def self.configuration; end
end

describe RedisCache do
  describe '.new' do
    let(:redis_url) {'redis://redis-host:9999'}

    configure {{redis: {url: redis_url}}}

    it "returns a Redis client" do
      assert RedisCache.new.is_a? Redis
    end

    it "passes the configuration url" do
      Redis.expects(:new).with(url: redis_url)
      RedisCache.new
    end
  end
end
