# frozen_string_literal: true

begin
  require './lib/application'
use(ViteRuby::DevServerProxy, ssl_verify_none: true) if ViteRuby.run_proxy?
rescue StandardError => e
  puts e
  exit(-1)
end

run RaidTrainer
