FROM ruby:3.3.2

ARG UID
ARG GID
ARG USERNAME=raidtrainer

RUN apt update
RUN apt install sqlite3 -y

RUN apt install sudo -y
RUN groupadd -g ${GID} ${USERNAME} \
    && useradd -r -u ${UID} -g ${USERNAME} --shell /bin/bash --create-home ${USERNAME} \
    && echo "${USERNAME} ALL=NOPASSWD: ALL" >> /etc/sudoers \
    && chown -R ${USERNAME} /usr/local

ADD ./Gemfile /app/src/Gemfile
ADD ./Gemfile.lock /app/src/Gemfile.lock

RUN chown ${USERNAME}:${USERNAME} -R /app/src

USER ${USERNAME}
WORKDIR /home/${USERNAME}

# Install nvm
ARG NODE_VERSION=20.15.0
ARG NVM_VERSION=0.39.7
ARG NVM_DIR=/home/${USERNAME}/.nvm
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v${NVM_VERSION}/install.sh | bash \
    && . $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default

# Install node libraries
WORKDIR /app/src
RUN . $NVM_DIR/nvm.sh \
    && npm install

WORKDIR /app/src
RUN gem install bundler
RUN bundle install

COPY --chown=${USERNAME}:${USERNAME} . /app/src

CMD ./start.sh
