#!/bin/env ruby
# frozen_string_literal: true

require './lib/application'

require 'date'
require 'csv'
data = CSV.parse(File.read('requests.csv'))[1..]

event_start = DateTime.now + 100.years
event_end = DateTime.now - 100.years

data.each do |row|
  start = DateTime.iso8601(row[3])
  final = DateTime.iso8601(row[4])

  event_start = [event_start, start].min
  event_end = [event_end, final].max
end

event_start -= 30.minutes
event_end += 30.minutes

organizer = Actor.find_by(name: 'wilkie') || Actor.create!(
  name: 'wilkie',
  handle: 'wilkiefaerie',
  timezone: 'EST'
)

organizer.links.find_by(service: 'twitch') || Link.create!(
  actor: organizer,
  service: 'twitch',
  url: 'https://twitch.tv/wilkiefaerie',
  handle: 'wilkiefaerie'
)

f = File.open('raidtrain_rough.png')
f2 = File.open('memphis_small.jpg')
event_name = 'Twitch Women\'s Guild Raid Train'
event = Event.find_by(name: event_name) || Event.create!(
  owner: organizer,
  service: 'twitch',
  start: event_start,
  end: event_end,
  image: f,
  background: f2,
  description: 'Raid train!',
  name: event_name,
  open: true,
  hidden: true,
  requests_visible: true,
  code: 'HDNCDE'
)

data.each do |row|
  name = row[1].gsub(' ', '')
  start = DateTime.iso8601(row[3])
  final = DateTime.iso8601(row[4])
  puts "#{name}: #{start}, #{final}"

  event_start = [event_start, start].min
  event_end = [event_end, final].max

  actor = Actor.find_by(
    handle: name
  )

  actor ||= Actor.create!(
    name:,
    handle: name
  )

  actor.links.find_by(service: event.service) || puts("creating #{name}") || Link.create!(
    actor:,
    service: 'twitch',
    url: "https://twitch.tv/#{name}",
    handle: name
  )

  Request.find_by(start:, end: final, actor:) || Request.create!(
    event:,
    actor:,
    start:,
    end: final
  )
end
