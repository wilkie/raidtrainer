# frozen_string_literal: true

source 'https://rubygems.org'
ruby '3.3.2'

# Web Framework
gem 'sinatra', '~> 3.0.0'
gem 'sinatra-contrib', '~> 3.0.0', require: false

# Markup Rendering Engine
gem 'redcarpet', git: 'https://github.com/vmg/redcarpet' # Markdown
gem 'slim', '~> 4.0.0' # Slim

# Internationalization
gem 'i18n'         # Main localization library
gem 'rails-i18n',  # Rails oriented default localizations
    require: nil #  (gives us default time/date localization)

# Database Access
gem 'activerecord'
gem 'sinatra-activerecord', require: 'sinatra/activerecord'
gem 'sqlite3', '~> 1.4'

# Front-end
gem 'vite_ruby'

# Currency
gem 'money'

# Cache
gem 'redis'

# File uploading
gem 'carrierwave'

# Annotate models
gem 'annotate'

# Sass (Stylesheet Format)
gem 'rack-sassc'
gem 'sassc'

# Web Server
gem 'puma'

# QR Code Generator
gem 'rqrcode'

# Authentication
gem 'omniauth'
gem 'omniauth-twitch', git: 'https://github.com/WebTheoryLLC/omniauth-twitch'

# Ruby Documentation
gem 'yard'

# Ruby linting
gem 'rubocop'

# API Documentation
gem 'yard-sinatra', git: 'https://github.com/rkh/yard-sinatra'

group :test, optional: true do
  gem 'cucumber'
  gem 'capybara', require: 'capybara/dsl'
  gem 'capybara-selenium'
  gem 'webdrivers'
  gem 'mocha', '~> 2.4.0' # stubs
  gem 'poltergeist'
  gem 'pry-byebug'
  gem 'rack-test', '~> 0.8.3', require: 'rack/test'
  gem 'selenium-webdriver'
  gem 'minitest'
  gem 'minitest-around'

  # Javascript testing
  gem 'jasmine'
end
