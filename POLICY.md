# Data Policy

## Twitch Authentication

When logging in via Twitch, we store only public information. We store the
handle, a convenient URL to the stream page itself, and pull the initial bio
from the Twitch account page.

We authenticate via OAuth2, a very standard method. We ask for a "read" token
to the account. This gives us access to only a variety of account information
and no means to affect the account. We throw away anything otherwise personally
identifiable not mentioned above, including any email addresses.

Due to this lack of storing such things, recovering an account may be impossible
if a Twitch handle changes. However, the authentication does store an identifier
understood only by Twitch that can reassociate a Twitch account with the profile
on this site. When your handle changes, you may update the profile here by simply
logging back in.
