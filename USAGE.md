# Using the Website

## Event Organizers

On your event page, you, and any co-owners, will have a navigation bar that will be hidden for all other viewers of the page.
From this bar, you can access pages that allow you to edit your event or view other information about the event.

![This shows the navigation bar depicting a set of buttons including those described below.](docs/screens/nav_screen.png)

Here is a breakdown of the options:

* **View Event**: Go to the event page
* **Edit Event**: Edit the main details of the event, such as the time
* **Request Sheet**: See the slot requests from would-be participants
* **Benefits List**: Add and view existing donation links
* **Admin**: Only the event owner can view this. It allows adding 'co-owners' and the ability to transfer main ownership to another party.

### Event Overview

The event page itself is the view others will see when they visit your event.
From here, there is a basic information panel with the name and dates of your event.
Below that is the expanded description along with a participants list.
While the event is occurring, these panels will be interrupted by the embedding of any active stream represented in the event schedule.
The schedule itself is presented toward the bottom.

![This shows a listing of participants after the heading reading 'About' and after the event description.](docs/screens/about_screen.png)

One can get to the first scheduled slot for a particular streamer by interacting with their name within the participants list.
This list is alphabetically sorted and represents the particular actors that have slots within the event.
To create a participant within a slot in the schedule, you would make a slot that ran for just a minute or less.
Slots that only run for that short amount of time are not listed on the schedule, but still contribute to the participants list.
New slots are created by interacting with the "New Slot" button in the schedule or by creating a slot from a Request.
The information in this section is edited via the "Edit Event" button in the navigation at the top of the page.

### Event Settings

The "Edit Event" page will allow you to update some important settings related to the event itself.

![This shows the heading 'Information' and under that a set of fields. There is a name field, a start and end time field, an image, a background image, a large box for a description, and a large box for a raid message, and then a set of checkboxes.](docs/screens/event_edit_screen.png)

The form above depicts the whole of those settings. Here is a breakdown of each:

* **Name**: The name of the event. This is prominently displayed at the top of most pages.
* **Start Time**: The initial time the event officially starts. It could be before the first scheduled slot.
* **End Time**: The time the event is officially closed. It might be after the last slot ends.
* **Image**: The main image for the event. This is shown on the event page and serves as a link to the embedded stream.
* **Background Image**: You can elect to see the background image on the event page. It will be tiled behind the event information.
* **Description**: A '[Markdown](https://www.markdownguide.org/cheat-sheet/)'-powered description of the event that will be displayed in the "About" section.
* **Raid Message**: A short message (can contain words that result in Twitch Emotes) that is a unified way of shouting a raid during the event. Copyable via a link beneath the embedded stream.
* **Event Hidden from Public**: Hides the event from the public listing. It cannot be visited at all except by owners, co-owners, or scheduled participants.
* **Allow Requests**: Enables the request link to allow folks to request time in the event.
* **Requests are Visible to All Participants**: Shows all pending requests to those making a new request. This is to allow folks to see the gaps in the schedule and offer that time.
* **Allow Participants to Edit Their Scheduled Time**: When unchecked, only owners and co-owners manage the time of slots. This allows a scheduled participant the ability to change when they are scheduled.
* **Show Intermissions**: In "Raid Train" style events, the slots are assumed to be consecutive. When checked, this visualizes the gap in the schedule as an "Intermission". This does nothing for "General" events.

### Event Requests

While organizing the event, the Request Sheet is a place to allow others to ask to participate in your event.
As they do so, they add "Requests" to this page.
From here, you as an owner or co-owner of the event can see the requests (possibly many different time slot options) and turn those into scheduled "slots" in the event itself.

By default, requests for the event are turned off.
When you want to open the event for requests (and then subsequently turn them off again), just go to "Edit Event" and check the box next to "Allow Requests".

You can organize an event before making it visible to the public.
You generally want to control who can request time in the event.
To do this, there is a mechanism to grant partial visibility to the event in order to request a slot.
This is done through the Request Link, which contains a short "password" embedded in the link.
This link works for everybody equally, so take care in how you hand it out.

![This shows the main information panel for the requests page. It has a header called 'Index' and there is a slightly complex link that it says is used to request signup. Below that are a list of slotted people and unslotted people.](docs/screens/requests_index_screen.png)

As seen in the image above, that link is provided in the info block.
This information panel also contains a useful breakdown of people who have requested a slot in the schedule.
The Slotted group are folks that made at least one request and are on the schedule.
The Unslotted group are the remaining folks who made a request but have not been accommodated.
Clicking on any name in this section will jump you to the part of the page with their requested times.

When a person follows the request link, they will be presented with the option of logging into the site, and having done so they will connect their Twitch account to the scheduler site itself.
This will bring in their bio and image automatically.
Next, they will be presented with the ability to request a time slot, which consists of a start and, optionally, end time, and the ability to include a note and a description of the content they wish to create during that time.

After they do this, you as the event organizer will see that request on the request listing page under that information panel.
It groups together all requests by the same actor.

![This shows the requests by the streamer SomaGreen. It shows a few requests for different times and dates listing them by their days as a header and then times as columns. Their avatar image and handle are prominent in each row. There are buttons marked "To Slot" and "Delete" next to each requested time.](docs/screens/requests_show_screen.png)

If you wish to create the slot from the request, you can press the "To Slot" button which just presents a form to create a Slot in the schedule where it has prefilled in the fields with the information provided by the participant.
You can elect to alter the fields as you see fit before creating.
When you create the slot, it is immediately available on the schedule for the event.
There is no way of notifying the participant, however, outside of them checking manually.
You may wish to notify participants of the schedule when it is complete.

***Note***: If you mark the 'Allow Participants to Edit Their Own Time' in the "Edit Event" settings, participants can alter their own scheduled time on their own.
Otherwise, slot participants can always modify their own bio, stream description, and upload an image to represent themselves on their scheduled slot once it is in the schedule.
Slot participants can also view a hidden event once they are accepted on to the schedule.
These are good things to remind participants so as to relieve pressures and responsibilities on and of organizers to update information on this case-by-case basis.

To dismiss a request and remove it from the board, use the "Delete" button.

![This shows the header reading 'Calendar' and then an empty checkbox reading 'Show filled in slots' followed by a table with times along the top edge and dates along the vertical edge. The cells within a simple solid colors ranging from light green to dark green. Some cells are white. There is a block of text alongside one of the cells showing streamer handles and time ranges.](docs/screens/requests_calendar_screen.png)

Finally, just after the information panel and above the information on each requested time slot there is a panel consisting of a heatmap widget of all of the requested slots.
This can sometimes be an overwhelming chart, but it helps in a pinch to find gaps in the schedule and who might fill them.
The areas are colored green and their darkness depicts areas with a lot of overlap.
The areas that are white have nobody to account for them and the areas of lightest green are areas that the fewest people have requested.
When filling out a schedule, the least contentious places (lightest green) are a sensible place to start.

The checkbox above the colored chart will allow you to overlap the heatmap with the schedule itself.
You can see the gaps in the schedule and quickly get a list of people who might fill it.
By hovering over any green area, it will pop up a simple box containing the names of people are are requesting that slot.
It accounts for 30 minutes at a time in terms of showing overlap.

### Event Benefits

Event owners and co-owners can elect to add one or more 'benefits' panels.
These panels attach themselves to donation campaign efforts maintained elsewhere.
Currently, the software supports only links to Tiltify and uses the Tiltify API to maintain the amount raised on the event page and to provide a donation button.

To create a benefit, visit the "Benefits List" area via the navigation at the top of the page.

![This shows the benefits panel which shows the "Add Benefit" link on that page.](docs/screens/benefits_listing_screen.png)

Click the "Add Benefit" link to go to a page where you can describe the donation campaign.

![This shows the new benefit form which depicts the various fields useful in describing the benefit including its URL, a donation URL, a description, and a name.](docs/screens/benefits_new_screen.png)

Although a complicated form, you only need to specify the donation campaign URL.
The other fields are provided to override anything it would get from visiting the main information page of the linked donation campaign.
For most cases, leaving the rest of the form blank is sufficient as it will fill that out with the information it finds on the third-party site.

Again, the only third-party we support at the moment is Tiltify.
By providing a Tiltify link, either to a team campaign or a user campaign, it will link the event to that campaign.
Only links in the following two forms are known to work at the moment (where `@user`, `+team` and `campaign-name` are substituted with the real names of the user, team, and campaign respectively):

* `https://tiltify.com/@user/campaign-name`
* `https://tiltify.com/+team/campaign-name`

You can get these URLs by simply visited the campaign on Tiltify and copying the URL for the main page of that campaign matching one of the URLs above and pasting it into the URL field when creating a new benefit.

***Note***: The 'Donation URL' field is the link to use for the donation button, not for the event itself. Again, this can be left blank, and it will be filled in with the Tiltify donation page for the campaign.

When you complete this and add the benefit, it will appear on the event page near the embedded stream. It will look something like this:

![This shows the benefits panel which shows the "Add Benefit" link on that page.](docs/screens/benefits_show_screen.png)

The donation button will open the donation URL in another tab. If you want it to go to a specific place, edit the benefit and replace the donation URL with that link.
Otherwise, leave it blank to have it direct, by default, to the donation page for the campaign on Tiltify itself.

### Event Administration

Event owners have an extra set of options on the "Admin" page.

![This shows a panel with the heading 'Owned By' with an actor named Meadow underneath. Then, there is a separate panel with the heading 'Transfer Ownership' with a simple text input marked 'handle' with a button underneath.](docs/screens/event_admin_screen.png)

Here, they can transfer the ownership of the event to another person.
There can only be one owner for an event, so this is a permanent action that cannot be reversed by the former owner, so they must take caution when doing this.

The safer option is to add Co-Owners.
A Co-Owner has most abilities to edit an event including editing the main settings and creating and editing slots in the schedule.
Co-Owners can see every page and setting for the event excluding the Admin page.
Therefore, co-owners cannot see who else are co-owners, nor can they alter the co-owner listing or transfer ownership of the event.

![This shows a panel with the heading 'Manage Co-Owners' with several folks listed underneath. Before that listing, there is a simple text field labeled 'Handle' and a button. Next to each person listed via their avatar and handle is a button marked 'Delete'.](docs/screens/event_admin_screen.png)

To add a new co-owner, simply add their handle to the text field and press the button.
The page will refresh and you should see them in the listing.
To remove a co-owner, press the delete button next to their handle.
This will immediately remove their access to the event.
It is wise for the owner to potentially add themselves as a co-owner.
When you transfer ownership, this is the only way to maintain some administrative access to the event.
If you fail to do so, the new owner would need to add you back as a co-owner.

If there is any need to transfer the ownership of an event outside of the current owner's ability, a site administrator can do this.
However, there is no ability on the site, currently, to know who is an administrator and how to contact them to make this request.

## Site Administrators

Information about how to use the website as an administrator can be found in the [DEPLOYING.md](DEPLOYING.md) documentation alongside the process to deploy your own copy of the website as these roles are typically related.

Creating site administrators is similarly only possible by those with direct access to the running server and not an option to those only remotely interacting with the actual website.
