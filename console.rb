#!/bin/env ruby
# frozen_string_literal: true

require_relative 'lib/application'
require 'irb'

ARGV.clear
IRB.start
