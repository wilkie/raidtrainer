# frozen_string_literal: true

# This is common code found elsewhere.
#
# these methods are already present in Active Support
unless Kernel.respond_to? :silence_warnings
  # Main standard library class for Ruby
  module Kernel
    def silence_warnings(&)
      with_warnings(nil, &)
    end

    def with_warnings(flag)
      old_verbose = $VERBOSE
      $VERBOSE = flag
      yield
    ensure
      $VERBOSE = old_verbose
    end
  end
end
