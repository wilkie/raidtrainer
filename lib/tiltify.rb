# frozen_string_literal: true

# This wraps all functionality around polling Tiltify data
class Tiltify
  SLUG_BASE_QUERY = <<-QUERY
    query OPERATION($vanity: String!, $slug: String!) {
      TYPE(vanity: $vanity, slug: $slug) {
        publicId
        legacyCampaignId
        name
        slug
        status
        __typename
      }
    }
  QUERY

  SLUG_CAMPAIGN_QUERY = SLUG_BASE_QUERY.gsub('OPERATION',
                                             'get_campaign_by_vanity_and_slug').gsub('TYPE',
                                                                                     'campaign').freeze
  SLUG_TEAM_EVENT_QUERY = SLUG_BASE_QUERY.gsub('OPERATION',
                                               'get_team_event_by_vanity_and_slug').gsub('TYPE',
                                                                                         'teamEvent').freeze

  # Issues a request to the service to get a bearer token
  def self.request_token
    uri = URI('https://v5api.tiltify.com/oauth/token')
    params = {
      client_id: RaidTrainer.configuration[:tiltify][:client_id],
      client_secret: RaidTrainer.configuration[:tiltify][:client_secret],
      grant_type: 'client_credentials',
    }

    Net::HTTP.post_form(uri, params)
  end

  # Returns a bearer token to be used as a client credential
  def self.bearer_token
    if !defined?(@token) || (@token_expires < DateTime.now)
      response = request_token
      return unless response.is_a?(Net::HTTPSuccess)

      token_data = JSON.parse(response.body)
      token = token_data['access_token']
      return unless token

      @token_expires = DateTime.now + token_data['expires_in'].seconds
      @token = token
    end

    @token
  end

  def self.uri_for(base, params = nil)
    uri = URI(base)
    uri.query = URI.encode_www_form(params) unless params.nil?
    uri
  end

  def self.get(uri)
    Net::HTTP.get_response(uri, {
                             Authorization: "Bearer #{bearer_token}",
                             'Client-Id': RaidTrainer.configuration[:tiltify][:client_id],
                           })
  end

  def self.post(uri, json)
    Net::HTTP.post(uri, json.to_json, {
                     Authorization: "Bearer #{bearer_token}",
                     'Client-Id': RaidTrainer.configuration[:tiltify][:client_id],
                     'Content-Type': 'application/json',
                     Accept: '*/*',
                   })
  end

  def self.get_graphql(operation, variables, query)
    uri = uri_for('https://api.tiltify.com/gql')
    post(uri, {
           operationName: operation,
           variables:,
           query:,
         })
  end

  def self.team_event_id_for_slug(vanity, slug)
    response = get_graphql('get_team_event_by_vanity_and_slug', { vanity:, slug: }, Tiltify::SLUG_TEAM_EVENT_QUERY)
    return unless response.is_a?(Net::HTTPSuccess)

    JSON.parse(response.body)['data']['teamEvent']['publicId']
  end

  def self.campaign_id_for_slug(vanity, slug)
    response = get_graphql('get_campaign_by_vanity_and_slug', { vanity:, slug: }, Tiltify::SLUG_CAMPAIGN_QUERY)
    return unless response.is_a?(Net::HTTPSuccess)

    JSON.parse(response.body)['data']['campaign']['publicId']
  end

  def self.retrieve_info_for(category, id)
    uri = uri_for("https://v5api.tiltify.com/api/public/#{category}/#{id}")
    response = get(uri)
    return unless response.is_a?(Net::HTTPSuccess)

    JSON.parse(response.body)['data']
  end

  def self.retrieve_info_for_url(url)
    # Get the slug from the url
    # e.g. https://tiltify.com/+the-four-directions/nahm-fundraiser
    # contains a team: 'the-four-directions' and the name of the fundraiser.
    vanity, slug = URI(url).path[1...].split('/')

    if vanity.start_with?('+')
      id = team_event_id_for_slug(vanity, slug)
      ret = retrieve_info_for(:team_campaigns, id)
    else
      id = campaign_id_for_slug(vanity, slug)
      ret = retrieve_info_for(:campaigns, id)
    end

    ret['cause'] = retrieve_info_for(:causes, ret['cause_id']) unless ret.blank?
    ret
  end
end
