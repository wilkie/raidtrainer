# frozen_string_literal: true

class RaidTrainer
  # This module is the base class for a route handler.
  class Controller < Sinatra::Base
    # Establish the environment
    set :environment, (ENV['RACK_ENV'] || 'development').intern

    # Use root directory as root
    set app_file: '.'
    set :server, :puma

    # GZIP
    use Rack::Deflater

    # Chunk responses with unknown Content-Length
    use Rack::Chunked

    # Static Asset Management
    set :static_cache_control, [:'app/public', { max_age: 60 * 60 * 24 * 365 }]
    set :public_folder, 'app/public'

    # We want to catch errors with the error handlers
    set :show_exceptions, false

    # Set views path
    root_path = File.realpath(File.join(File.dirname(__FILE__), '..'))
    set :slim, views: File.join(root_path, 'app', 'views')

    # Markdown
    Tilt.register Tilt::RedcarpetTemplate, 'md'
    Tilt.prefer   Tilt::RedcarpetTemplate
    set :markdown, layout_engine: :slim,
                   layout: :markdown,
                   fenced_code_blocks: true

    use Rack::MethodOverride

    # Helpers
    helpers Sinatra::ContentFor

    # Reloader
    configure :development do
      puts '***** In Development Mode *****'

      silence_warnings do
        require 'sinatra/reloader'
      end

      # Possibly import Pry (debugger)
      begin
        require 'pry'
        puts 'Debugging via Pry enabled.'
      rescue LoadError
        puts 'Debugging via Pry disabled (Pry not installed).'
      end

      puts 'Using Reloader'

      register Sinatra::Reloader

      %w[helpers models controllers].each do |dir|
        Dir[File.join(File.dirname(__FILE__), '..', 'app', dir, '**', '*.rb')].each do |file|
          also_reload file
        end
      end

      puts '*******************************'
    end

    helpers do
      def partial(page, options = {}, &)
        page = if page.to_s.include? '/'
                 page.to_s.sub(%r{/([^/]+)$}, '/_\\1')
               else
                 "_#{page}"
               end

        render(:slim, page.to_sym, options.merge!(layout: false), &).to_s
      end

      def markdown(path, options = {})
        path = "#{settings.slim[:views]}/#{path}.md"
        markdown = options[:markdown] || File.read(path)
        options.update({ fenced_code_blocks: true })
        engine = ::Redcarpet::Markdown.new(::Redcarpet::Render::HTML, options)
        engine.render(markdown)
      rescue StandardError
        ''
      end
    end

    # 404 route
    not_found do
      # Do not GZIP the 404 page
      cache_control 'no-transform'

      render(:slim, :'static/404') if body.nil? || body.empty?
    end

    # 500 route.
    error do
      # Do not GZIP the 500 page
      cache_control 'no-transform'

      render(:slim, :'static/500') if body.nil? || body.empty?
    end

    # When records are not found, return a 404
    error ActiveRecord::RecordNotFound do |e|
      # Do not GZIP the 404 page
      cache_control 'no-transform'

      status 404
      render(:slim, :'static/404', locals: { error: e })
    end
  end
end
