# frozen_string_literal: true

# RaidTrainer - Collaborative Stream Event Planning and Presentation
# Copyright (C) 2023 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative 'silence_warnings'

silence_warnings do
  require 'rbconfig'
  require 'em/pure_ruby' if RbConfig::CONFIG['host_os'] =~ /mswin|mingw|cygwin/

  require 'bundler'
  Bundler.require

  require 'sinatra'
  require 'sinatra/contrib'

  require 'carrierwave'
  require 'carrierwave/orm/activerecord'
end

# Application root.
class RaidTrainer < Sinatra::Base
  # Establish the environment
  set :environment, (ENV['RACK_ENV'] || 'development').intern

  # We want to catch errors with the error handlers
  set :show_exceptions, false

  # This module contains the many custom helpers for the app
  module Helpers
  end

  # This module containers route handlers
  module Controllers
  end

  def self.config_sample_file_path
    return ENV['RAIDTRAINER_CONFIG_BASE'] if ENV['RAIDTRAINER_CONFIG_BASE']
    File.join(File.dirname(__FILE__), '..', 'config.sample.yml')
  end

  def self.config_file_path
    File.join(File.dirname(__FILE__), '..', 'config.yml')
  end

  def self.read_configuration_file
    db_config_file_path = File.join(File.dirname(__FILE__), '..', 'config', 'database.yml')

    ret = YAML.load_file(config_file_path) || {}

    # Combine the database configuration
    db = YAML.load_file(db_config_file_path)
    db = db[RaidTrainer.environment.intern] || {}

    ret[:database] ||= {}
    ret[:database].update(db)

    ret.with_indifferent_access
  end

  def self.write_new_configuration_file
    # Get the sample config
    sample_config = File.read(config_sample_file_path)

    # Set the secret
    secret = SecureRandom.hex(32)
    sample_config.gsub!('"<!SECRET>"', secret)

    # Write it out
    File.open(config_file_path, 'w+') do |f|
      f.puts(sample_config)
    end
  end

  # Gather configuration
  def self.configuration
    write_new_configuration_file unless File.exist?(config_file_path)

    @configuration ||= read_configuration_file
  end

  # Use sessions

  # Determine the 'secure' attribute status of the session
  secure_on = RaidTrainer.configuration['secure-session']

  # If the configuration does not specify specifically true/false, then we
  # default to true if and only if it is production mode.
  secure_on = RaidTrainer.production? if secure_on.nil?

  set :session_secret, RaidTrainer.configuration[:secret]
  use Rack::Session::Cookie,
    key: 'rack.session',
    secret: RaidTrainer.configuration[:secret],
    domain: RaidTrainer.configuration[:domain],
    expire_after: 2_592_000,
    # We cannot have this because omniauth fails to redirect properly
    # It would be good if the omniauth cookies did not have a SameSite set
    #same_site: true,
    secure: secure_on

  # OmniAuth
  use OmniAuth::Builder do
    provider(:developer, fields: [:name], uid_field: :name) if development?

    if RaidTrainer.configuration[:twitch]&.[](:client_id)
      provider :twitch, RaidTrainer.configuration[:twitch][:client_id],
               RaidTrainer.configuration[:twitch][:client_secret], provider_ignores_state: true, scope: 'user:read:email'
    end
  end

  # Carrierwave
  CarrierWave.configure do |config|
    config.root = -> { File.join(File.dirname(__FILE__), '..', 'app', 'public') }
    config.storage = :file

    if development?
      config.ignore_integrity_errors = false
      config.ignore_processing_errors = false
      config.ignore_download_errors = false
    end

    if test?
      config.storage = :file
      config.enable_processing = false
    end
  end

  # I18n
  I18n.load_path += Dir[File.join(File.dirname(__FILE__), '..', 'locales', '**', '*.yml')]
  I18n.load_path += Dir[File.join(Gem::Specification.find_by_name('rails-i18n').gem_dir, 'rails', 'locale', '*.yml')]
  I18n.default_locale = :en

  use Rack::SassC, {
    check: !RaidTrainer.production?,
    syntax: :scss,
    css_location: 'app/public/css',
    scss_location: 'app/stylesheets',
    create_map_file: true,
  }
end

# The base controller
require_relative 'controller'

# Load base application source
Dir[File.join(File.dirname(__FILE__), '**', '*.rb')].each do |file|
  require_relative file
end

# Load application source
%w[helpers uploaders models].each do |dir|
  Dir[File.join(File.dirname(__FILE__), '..', 'app', dir, '**', '*.rb')].each do |file|
    require_relative file
  end
end

# Load application routes
%w[controllers].each do |dir|
  Dir[File.join(File.dirname(__FILE__), '..', 'app', dir, '**', '*.rb')].each do |file|
    require_relative file
  end
end

# Ensure the css destination exists
css_path = File.join(File.dirname(__FILE__), '..', 'app', 'public', 'css')
unless File.exist?(css_path)
  Dir.mkdir(css_path)
end

# Application root.
class RaidTrainer
  module Controllers
    # The 400/500 controller
    class ErrorController < RaidTrainer::Controller
      # 404 route
      not_found do
        # Do not GZIP the 404 page
        cache_control 'no-transform'

        render(:slim, :'static/404')
      end

      # 500 route.
      error do
        # Do not GZIP the 500 page
        cache_control 'no-transform'

        render(:slim, :'static/500')
      end
    end
  end

  use Controllers::ErrorController

  def call(env)
    # Allows root not_found/error pages
    RaidTrainer::Controllers::ErrorController.call(env)
  end
end
