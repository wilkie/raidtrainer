# frozen_string_literal: true

# This wraps all functionality around the Twitch API
class Twitch
  # Issues a request to the service to get a bearer token
  def self.request_token
    uri = URI('https://id.twitch.tv/oauth2/token')
    params = {
      client_id: RaidTrainer.configuration[:twitch][:client_id],
      client_secret: RaidTrainer.configuration[:twitch][:client_secret],
      grant_type: 'client_credentials',
    }

    Net::HTTP.post_form(uri, params)
  end

  # Returns a bearer token to be used as a client credential
  def self.bearer_token
    if !defined?(@token) || (@token_expires < DateTime.now)
      response = request_token
      return unless response.is_a?(Net::HTTPSuccess)

      token_data = JSON.parse(response.body)
      token = token_data['access_token']
      return unless token

      @token_expires = DateTime.now + token_data['expires_in'].seconds
      @token = token
    end

    @token
  end

  def self.uri_for(base, params)
    uri = URI(base)
    uri.query = URI.encode_www_form(params)
    uri
  end

  def self.get(uri)
    # Get a bearer token
    token = bearer_token

    Net::HTTP.get_response(uri, {
                             Authorization: "Bearer #{token}",
                             'Client-Id': RaidTrainer.configuration[:twitch][:client_id],
                           })
  end

  def self.get_channel_info(id_or_ids)
    id_or_ids = [id_or_ids] unless id_or_ids.is_a?(Array)

    # Craft request
    params = { broadcaster_id: id_or_ids }
    res = get(uri_for('https://api.twitch.tv/helix/channels', params))
    return [] unless res.is_a?(Net::HTTPSuccess)

    data = JSON.parse(res.body)
    data['data']
  end

  def self.get_stream_info(id_or_ids)
    id_or_ids = [id_or_ids] unless id_or_ids.is_a?(Array)

    # Craft request
    params = { user_id: id_or_ids }
    res = get(uri_for('https://api.twitch.tv/helix/streams', params))
    return [] unless res.is_a?(Net::HTTPSuccess)

    data = JSON.parse(res.body)
    data['data']
  end

  # Get a dictionary full of profile information for the given Twitch account.
  def self.retrieve_user_info_by_handle(handle)
    params = { login: handle }
    res = get(uri_for('https://api.twitch.tv/helix/users', params))
    return unless res.is_a?(Net::HTTPSuccess)

    data = JSON.parse(res.body)
    data['data']&.first
  end
end
