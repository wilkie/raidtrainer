# Sets up a simple Redis client for handling cached pages and such.
class RedisCache
  def self.new
    Redis.new(url: RaidTrainer.configuration[:redis][:url])
  end
end
